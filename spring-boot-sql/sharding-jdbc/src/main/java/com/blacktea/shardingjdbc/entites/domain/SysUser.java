package com.blacktea.shardingjdbc.entites.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.blacktea.shardingjdbc.entites.BasicObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author blacktea
 * @since 2021-09-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_user")
public class SysUser extends BasicObject<SysUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 账号名称
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 出生日期
     */
    private Date dateBirth;

    /**
     * 1:男,2:女
     */
    private Integer sex;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
