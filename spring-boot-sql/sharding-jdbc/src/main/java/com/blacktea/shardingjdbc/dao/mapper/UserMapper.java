package com.blacktea.shardingjdbc.dao.mapper;

import com.blacktea.shardingjdbc.entites.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author blacktea
 * @since 2021-09-15
 */
public interface UserMapper extends BaseMapper<User> {

}
