package com.blacktea.shardingjdbc.dao.mapper;

import com.blacktea.shardingjdbc.entites.domain.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author blacktea
 * @since 2021-09-21
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
