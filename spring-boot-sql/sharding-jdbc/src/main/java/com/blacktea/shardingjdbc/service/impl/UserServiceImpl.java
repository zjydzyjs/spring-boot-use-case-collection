package com.blacktea.shardingjdbc.service.impl;

import com.blacktea.shardingjdbc.entites.domain.User;
import com.blacktea.shardingjdbc.dao.mapper.UserMapper;
import com.blacktea.shardingjdbc.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author blacktea
 * @since 2021-09-15
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
