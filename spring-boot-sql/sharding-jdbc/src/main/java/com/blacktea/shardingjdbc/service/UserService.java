package com.blacktea.shardingjdbc.service;

import com.blacktea.shardingjdbc.entites.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author blacktea
 * @since 2021-09-15
 */
public interface UserService extends IService<User> {

}
