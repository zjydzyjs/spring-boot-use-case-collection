package com.blacktea.shardingjdbc.service.impl;

import com.blacktea.shardingjdbc.entites.domain.SysUser;
import com.blacktea.shardingjdbc.dao.mapper.SysUserMapper;
import com.blacktea.shardingjdbc.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author blacktea
 * @since 2021-09-21
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
