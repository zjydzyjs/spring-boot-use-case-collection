package com.blacktea.shardingjdbc.service;

import com.blacktea.shardingjdbc.entites.domain.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author blacktea
 * @since 2021-09-21
 */
public interface SysUserService extends IService<SysUser> {

}
