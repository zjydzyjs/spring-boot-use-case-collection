package com.blacktea.shardingjdbc.entites.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.blacktea.shardingjdbc.entites.BasicObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author blacktea
 * @since 2021-09-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("user")
public class User extends BasicObject<User> {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
