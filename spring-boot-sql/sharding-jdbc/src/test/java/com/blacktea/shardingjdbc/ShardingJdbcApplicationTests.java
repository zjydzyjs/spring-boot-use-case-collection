package com.blacktea.shardingjdbc;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
class ShardingJdbcApplicationTests {

    @Test
    void contextLoads() {
    }
}
