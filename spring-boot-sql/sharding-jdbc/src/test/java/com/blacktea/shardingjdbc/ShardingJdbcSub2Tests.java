package com.blacktea.shardingjdbc;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blacktea.shardingjdbc.entites.domain.SysUser;
import com.blacktea.shardingjdbc.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

@SpringBootTest
@Slf4j
class ShardingJdbcSub2Tests {

    @Autowired
    private SysUserService sysUserService;

    public SysUser getUser(int ds_index,int table_index){
        SysUser sysUser = new SysUser();
        // ds 索引是根据 ds$->{sex % 2} # 分片算法表达式
        if (ds_index == 0){
            sysUser.setSex(2);
        }else {
            sysUser.setSex(1);
        }
        // table 索引 是根据 sys_user$->{age % 2} # 分片算法表达式
        if (table_index == 0){
            sysUser.setAge(22);
        }else {
            sysUser.setAge(23);
        }
        sysUser.setPassword("******");
        sysUser.setUserName("测试分库分表-"+sysUser.getSex()+"-"+sysUser.getAge());
        sysUser.setDateBirth(new Date());
        return sysUser;
    }

    @Test
//    @Transactional(rollbackFor = Exception.class)
    void createDs0_sysUser0(){
        SysUser user = this.getUser(0, 0);
        boolean save = sysUserService.save(user);
//        int i = 1/0;
        // 单库 Transactional 有效

        // dsl - log
        // 2021-09-22 14:51:08.898  INFO 6304 --- [           main] ShardingSphere-SQL                       : Actual SQL: ds0 ::: INSERT INTO sys_user0   (id, user_name, password, age, date_birth, sex) VALUES (?, ?, ?, ?, ?, ?) ::: [1440569333690953730, 测试分库分表-2-22, ******, 22, 2021-09-22 14:51:08.24, 2]
    }


    @Test
    void createDs1_sysUser0(){
        SysUser user = this.getUser(1, 0);
        boolean save = sysUserService.save(user);
    }

    @Test
    void createDs0_sysUser1(){
        SysUser user = this.getUser(0, 1);
        boolean save = sysUserService.save(user);
    }

    @Test
    void list(){
        // 从打印的sql 日志可以发现它是从配置中的所有 ds$.table$ 进行遍历查询,最后将结果汇总
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("sex",2);
        List<SysUser> list = sysUserService.list(queryWrapper);
        log.info("size:{},库查询全部数据:{}", list.size(), JSONUtil.toJsonStr(list));
    }

    @Test
    void page(){
        long current=1;
        long size=10;
        Page<SysUser> sysUserPage = new Page<>(current,size);
        do {
            sysUserPage = sysUserService.page(sysUserPage);
            List<SysUser> records = sysUserPage.getRecords();
            log.info("size:{},当前第{}页,数据:{}", records.size(), current,JSONUtil.toJsonStr(records));
            current++;
            sysUserPage.setCurrent(current);
        }while (!CollectionUtils.isEmpty(sysUserPage.getRecords()));

    }
}
