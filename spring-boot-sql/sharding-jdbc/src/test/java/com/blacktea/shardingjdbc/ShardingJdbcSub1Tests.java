package com.blacktea.shardingjdbc;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.blacktea.shardingjdbc.entites.domain.SysUser;
import com.blacktea.shardingjdbc.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

@SpringBootTest
@Slf4j
class ShardingJdbcSub1Tests {

    @Autowired
    private SysUserService sysUserService;

    public SysUser getUser(int ds_index,int table_index){
        SysUser sysUser = new SysUser();
        // ds 索引是根据 ds$->{sex % 2} # 分片算法表达式
        if (ds_index == 0){
            sysUser.setSex(2);
        }else {
            sysUser.setSex(1);
        }
        // table 索引 是根据 sys_user$->{age % 2} # 分片算法表达式
        if (table_index == 0){
            sysUser.setAge(22);
        }else {
            sysUser.setAge(23);
        }
        sysUser.setPassword("******");
        sysUser.setUserName("测试分库分表-"+sysUser.getSex()+"-"+sysUser.getAge());
        sysUser.setDateBirth(new Date());
        return sysUser;
    }

    @Test
//    @Transactional(rollbackFor = Exception.class)
    void createDs0_sysUser0(){
        SysUser user = this.getUser(0, 0);
        boolean save = sysUserService.save(user);
//        int i = 1/0;
        // 单库 Transactional 有效
    }

    @Test
    void createDs0_sysUser1(){
        SysUser user = this.getUser(0, 1);
        boolean save = sysUserService.save(user);
    }


    @Test
    void createDs1_sysUser0(){
        SysUser user = this.getUser(1, 0);
        boolean save = sysUserService.save(user);
    }

    @Test
    void createDs1_sysUser1(){
        SysUser user = this.getUser(1, 1);
        boolean save = sysUserService.save(user);
    }

    @Test
//    @Transactional(rollbackFor = Exception.class)
//    @ShardingTransactionType(TransactionType.XA)
    void batchAdd(){
        for (int i = 0; i < 10; i++) {
            if (i % 4 == 0){
                this.createDs0_sysUser0();
            }else if (i % 4 == 1){
                this.createDs0_sysUser1();
            }else if (i % 4 == 2){
                this.createDs1_sysUser0();
            }else {
                this.createDs1_sysUser1();
            }
        }
        //  @Transactional，这样跨库同样支持,不需要XA
//        int i = 1/0;
    }

    @Test
    void updateUser(){
        UpdateWrapper<SysUser> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("age",22);
        // 测试根据age的为22并修改该集合判断如何修改多库多表的?
        // 从打印的sql可以发现,它是从配置中的所有 ds$.table$ 进行符合策略遍历修改
        // 结果并不会,只在原表基础上更新数据！
        updateWrapper.set("age",30);
        sysUserService.update(updateWrapper);
        // 结论：如果你要修改该策略值(不再匹配策略故障那种)怎么办?
        // 例如： table=sys_user+ age % 2 -> sys_user0表示偶数,sys_user1表示奇数
        // 1. 假如 库0-sys_user0中的age=22,你要改成23,那么你应该要换到库0-sys_user1中
        // 不换的话,以后再根据age=22就再也查不到这条数据了进行更改了,因为它根本不会匹配到该表
        // 本质的话: 该表不应该存在不同于该分表或分库策略的记录,即使是修改出来的也不应该存在!
    }

    /**
     * 与修改存在一样的问题
     */
    @Test
    void delete(){
        QueryWrapper<SysUser> query = new QueryWrapper<>();
        query.eq("age",22);
        sysUserService.remove(query);
    }

    @Test
    void list(){
        // 从打印的sql 日志可以发现它是从配置中的所有 ds$.table$ 进行遍历查询,最后将结果汇总
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("age",22);
        List<SysUser> list = sysUserService.list(queryWrapper);
        log.info("size:{},库查询全部数据:{}", list.size(), JSONUtil.toJsonStr(list));
    }

    @Test
    void page(){
        long current=1;
        long size=10;
        Page<SysUser> sysUserPage = new Page<>(current,size);
        do {
            sysUserPage = sysUserService.page(sysUserPage);
            List<SysUser> records = sysUserPage.getRecords();
            log.info("size:{},当前第{}页,数据:{}", records.size(), current,JSONUtil.toJsonStr(records));
            current++;
            sysUserPage.setCurrent(current);
        }while (!CollectionUtils.isEmpty(sysUserPage.getRecords()));

    }
}
