package com.blacktea.shardingjdbc;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blacktea.shardingjdbc.entites.domain.User;
import com.blacktea.shardingjdbc.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
@Slf4j
class ShardingJdbcReadWriteTests {

    @Autowired
    private UserService userService;

    @Test
    void contextLoads() {
    }

    @Test
    void list(){
        List<User> list = userService.list();
        log.info("size:{},从库查询全部数据:{}", list.size(),JSONUtil.toJsonStr(list));
    }

    @Test
    @Transactional
    // 因为JUnit使用 @Transactional会自动开启事务,即没有报错也会执行事务,所以需要 @Rollback(false)关闭事务
    @Rollback(false)
    void add(){
        User user = new User();
        user.setName("读写分离添加!");
        log.info("主库添加结果:{}",userService.save(user));
//        int i = 1/0;
    }

    @Test
    void update(){
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("name","读写分离添加!");
        User user = userService.getOne(userQueryWrapper);
        user.setName("读写分离修改!");
        log.info("主库修改结果:{}",userService.updateById(user));
    }

    @Test
    void delete() throws Exception {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id",1440877837299494914L);
        log.info("主库删除结果:{}",userService.remove(queryWrapper));
    }
}
