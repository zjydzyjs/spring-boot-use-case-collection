package com.blacktea.webmagic.demo.processor;

import lombok.extern.slf4j.Slf4j;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;

import java.util.function.Consumer;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/24 10:16
 */
@Slf4j
public class CSDNItemArticleProcessor extends AbstractPageProcessor {

    public CSDNItemArticleProcessor(Site site, Consumer<Page> pageConsumer) {
        super(site, pageConsumer);
    }
}
