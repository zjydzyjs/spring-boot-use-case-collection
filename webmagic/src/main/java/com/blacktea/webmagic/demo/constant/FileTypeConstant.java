package com.blacktea.webmagic.demo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/26 11:11
 */
public class FileTypeConstant {

    @AllArgsConstructor
    @Getter
    public enum FileTypeEnum {
        /****/
        MD(".md","application/octet-stream"),
        ZIP(".zip","application/octet-stream"),
        PNG(".png","")
        ;
        private String fileType;
        private String contentType;
    }

    @AllArgsConstructor
    @Getter
    public enum DefaultFileEnum {
        /**  **/
        MD("默认MD",".md"),
        ZIP("默认ZIP",".zip")
        ;
        private String defaultFileName;
        private String defaultFileType;

        public String fileName(){
            return this.defaultFileName+this.defaultFileType;
        }
    }


}
