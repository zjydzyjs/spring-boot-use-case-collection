package com.blacktea.webmagic.demo.pipeline;

import cn.hutool.core.map.MapUtil;
import com.blacktea.webmagic.demo.domain.Download;
import com.blacktea.webmagic.demo.strategy.DownloadContext;
import com.blacktea.webmagic.demo.util.WebMagicUtil;
import lombok.extern.slf4j.Slf4j;
import us.codecraft.webmagic.ResultItems;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/27 11:06
 */
@Slf4j
public class ListDownloadPipeline extends AbstractDownloadPipeline{

    private final Download download;
    private final AtomicInteger indexInteger  = new AtomicInteger(0);
    private final AtomicInteger mdInteger = new AtomicInteger(0);
    private final AtomicInteger imgInteger = new AtomicInteger(0);

    public ListDownloadPipeline(Download download) {
        this.download = download;
    }

    @Override
    public void download(ResultItems resultItems) {
        Map<String, Map<String, String>> dataMap = WebMagicUtil.getDataMap(download, resultItems,indexInteger,imgInteger,mdInteger);
        DownloadContext.context(download).ifPresent(context -> {
            if (MapUtil.isNotEmpty(dataMap)){
                context.executDownloadZip2(download,dataMap);
            }
        });
    }
}
