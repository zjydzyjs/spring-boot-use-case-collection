package com.blacktea.webmagic.demo.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description: 详情页
 * @author: black tea
 * @date: 2021/9/27 9:45
 */
@Data
public class ItemPage extends Page{

    @ApiModelProperty(value = "目标页面url",required = true,example = "https://blog.csdn.net/weixin_43917143/article/details/120436445")
    private String itemPageUrl;
}
