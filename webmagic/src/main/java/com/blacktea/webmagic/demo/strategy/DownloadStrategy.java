package com.blacktea.webmagic.demo.strategy;

import com.blacktea.webmagic.demo.domain.Download;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/26 14:23
 */
public interface DownloadStrategy {

    /**
     * 校验参数策略
     */
    void check(Download download);

    void downloadMD(String pathorName, HttpServletResponse response , String html);

    void downloadZip(String pathorName, HttpServletResponse response , Map<String, Map<String, String>> dataMap);

    void downloadZip(ZipOutputStream zos ,Map<String, Map<String, String>> dataMap);
}
