package com.blacktea.webmagic.demo.pipeline;

import lombok.extern.slf4j.Slf4j;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

/**
 * @description:
 * @author: black tea
 * @date: 2021/7/19 14:54
 */
@Slf4j
public abstract class AbstractDownloadPipeline implements Pipeline {

    @Override
    public void process(ResultItems resultItems, Task task) {
       this.download(resultItems);
    }

    public abstract void download(ResultItems resultItems);

}
