package com.blacktea.webmagic.demo.condiction;

import cn.hutool.core.lang.Assert;
import com.blacktea.webmagic.demo.domain.BrowseDownload;
import com.blacktea.webmagic.demo.domain.ItemPage;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/26 13:53
 */
public class DownloadBrowseCondiction extends BaseDownloadCondiction<BrowseDownload>{

    @Override
    public void check(BrowseDownload download) {
        super.check(download);
        if (download.getPage() instanceof ItemPage){
            Assert.notNull(download.getResponse());
        }
    }
}
