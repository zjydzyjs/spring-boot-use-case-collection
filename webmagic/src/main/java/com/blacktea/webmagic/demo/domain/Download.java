package com.blacktea.webmagic.demo.domain;

import lombok.Data;

import java.util.zip.ZipOutputStream;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/24 16:45
 */
@Data
public abstract class Download {

    /** 下载时,需要对页面进行处理的内容,两种实现:{@link ListPage}、{@link ItemPage} **/
    private Page page;

    /** 下载时用于压缩所需要下载文件的zip流,多个节点(使用ListPage)时必传 **/
    private ZipOutputStream zipOutputStream;

}
