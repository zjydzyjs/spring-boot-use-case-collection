package com.blacktea.webmagic.demo.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description: 列表页
 * @author: black tea
 * @date: 2021/9/27 9:39
 */
@Data
public class ListPage extends Page{

    @ApiModelProperty(value = "提供需要目标地址的页面url,例如CSDN的列表页",required = true,example = "https://blog.csdn.net/weixin_43917143")
    private String helpUrl;

    @ApiModelProperty(value = "找寻目标地址的匹配url,暂时仅支持 regx 匹配",required = true,example = "(https://blog\\.csdn\\.net/weixin_43917143/article/details/\\w+)")
    private String targetUrl;

}
