package com.blacktea.webmagic.demo.service;

import com.blacktea.webmagic.demo.domain.ItemPage;
import com.blacktea.webmagic.demo.domain.ListPage;

import javax.servlet.http.HttpServletResponse;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/27 9:34
 */
public interface ExportService {

    void itemExportMd(ItemPage itemPage, HttpServletResponse response);

    void itemExportZip(ItemPage itemPage, HttpServletResponse response);

    void listExportZip(ListPage listPage, HttpServletResponse response);
}
