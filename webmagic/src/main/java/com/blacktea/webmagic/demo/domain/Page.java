package com.blacktea.webmagic.demo.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/27 9:43
 */
@Data
public abstract class Page {

    @ApiModelProperty(value = "下载的文件名称,要求带后缀文件类型",required = true,example = "csdn.zip")
    private String fileName;

    @ApiModelProperty(value = "目标页面需要匹配的内容字段条件",required = true,example = "[" +
            "    {" +
            "        \"key\": \"md\"," +
            "        \"value\": \"//*[@id=\"article_content\"]/[@id=\"content_views\"]\"," +
            "        \"type\": \"XPath\"," +
            "        \"fileType\": \"MD\"" +
            "    }" +
            "]")
    private List<Field> fields;
}
