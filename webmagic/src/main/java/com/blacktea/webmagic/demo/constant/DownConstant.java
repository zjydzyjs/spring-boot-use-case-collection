package com.blacktea.webmagic.demo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/26 11:01
 */
public class DownConstant {

    public final static FileTypeConstant.DefaultFileEnum DEFAULT_MD = FileTypeConstant.DefaultFileEnum.MD;
    public final static FileTypeConstant.DefaultFileEnum DEFAULT_ZIP = FileTypeConstant.DefaultFileEnum.ZIP;

    public final static String IMG_DATA_MAP = "IMG_DATA_MAP";
    public final static String MD_DATA_MAP = "MD_DATA_MAP";

    public class LocalDown {
        public final static String DEFAULE_PATH = "D:\\下载文件\\chrome\\爬虫\\download\\CSDN\\";
    }

    @AllArgsConstructor
    @Getter
    public enum RequestHeadEnum {
        /****/
        USER_AGENT("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36")

        ;
        private String key;
        private String value;

    }

}
