package com.blacktea.webmagic.demo.service;

import cn.hutool.core.io.IoUtil;
import com.blacktea.webmagic.demo.constant.FileTypeConstant;
import com.blacktea.webmagic.demo.domain.BrowseDownload;
import com.blacktea.webmagic.demo.domain.ItemPage;
import com.blacktea.webmagic.demo.domain.ListPage;
import com.blacktea.webmagic.demo.pipeline.ItemDownloadPipeline;
import com.blacktea.webmagic.demo.pipeline.ListDownloadPipeline;
import com.blacktea.webmagic.demo.pipeline.MDDownloadPipeline;
import com.blacktea.webmagic.demo.processor.CSDNItemArticleProcessor;
import com.blacktea.webmagic.demo.processor.CSDNListProcessor;
import com.blacktea.webmagic.demo.util.WebMagicUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.zip.ZipOutputStream;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/27 9:34
 */
@Service
@Slf4j
public class ExportServiceImpl implements ExportService{

    @Override
    public void itemExportMd(ItemPage itemPage, HttpServletResponse response) {
        BrowseDownload browseDownload = new BrowseDownload();
        browseDownload.setResponse(response);
        browseDownload.setPage(itemPage);
        CSDNItemArticleProcessor csdnItemArticleProcessor = (CSDNItemArticleProcessor) WebMagicUtil.csdnProcess(browseDownload,false);
        WebMagicUtil.create(csdnItemArticleProcessor,itemPage.getItemPageUrl(),5,  MDDownloadPipeline.download(browseDownload))
                .run();
    }

    @Override
    public void itemExportZip(ItemPage itemPage, HttpServletResponse response) {
        BrowseDownload browseDownload = new BrowseDownload();
        browseDownload.setPage(itemPage);
        browseDownload.setResponse(response);
        CSDNItemArticleProcessor csdnItemArticleProcessor = (CSDNItemArticleProcessor) WebMagicUtil.csdnProcess(browseDownload,true);
        WebMagicUtil.create(csdnItemArticleProcessor,itemPage.getItemPageUrl(),5, ItemDownloadPipeline.download(browseDownload))
                .run();
    }

    @Override
    public void listExportZip(ListPage listPage, HttpServletResponse response) {


        try {
            // 导出执行
            BrowseDownload browseDownload = new BrowseDownload();
            browseDownload.setPage(listPage);
            ServletOutputStream out = response.getOutputStream();
            ZipOutputStream zipOutputStream = new ZipOutputStream(out);
            browseDownload.setZipOutputStream(zipOutputStream);
            CSDNListProcessor csdnListProcessor = (CSDNListProcessor) WebMagicUtil.csdnProcess(browseDownload, true);
            ListDownloadPipeline listDownloadPipeline = new ListDownloadPipeline(browseDownload);
            WebMagicUtil.create(csdnListProcessor,listPage.getHelpUrl(),listDownloadPipeline).run();
            zipOutputStream.close();
            FileTypeConstant.FileTypeEnum zip = FileTypeConstant.FileTypeEnum.ZIP;
            response.setContentType(zip.getContentType());
            response.setHeader("Content-Disposition", "attachment;filename="+ URLEncoder.encode(listPage.getFileName(), "UTF-8"));
            out.flush();
            IoUtil.close(out);
        } catch (IOException e) {
            log.error("list-导出失败!",e);
        }
    }
}
