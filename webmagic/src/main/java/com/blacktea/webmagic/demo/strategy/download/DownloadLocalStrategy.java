package com.blacktea.webmagic.demo.strategy.download;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ZipUtil;
import com.blacktea.webmagic.demo.condiction.DownloadLocalCondiction;
import com.blacktea.webmagic.demo.constant.DownConstant;
import com.blacktea.webmagic.demo.domain.Download;
import com.blacktea.webmagic.demo.domain.LocalDownload;
import com.blacktea.webmagic.demo.strategy.DownloadStrategy;
import com.blacktea.webmagic.demo.util.WebMagicUtil;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/26 14:28
 */
@Slf4j
public class DownloadLocalStrategy implements DownloadStrategy {

    @Override
    public void check(Download download) {
        new DownloadLocalCondiction()
                .check((LocalDownload) download);
    }

    @Override
    public void downloadMD(String pathorName, HttpServletResponse response, String html) {
        try {
            boolean exist = FileUtil.exist(pathorName);
            if (!exist){
                FileUtil.mkParentDirs(pathorName);
            }
            FileWriter writer = new FileWriter(pathorName);
            writer.write(html);
            log.info("\n当前下载文件到-{}完成。",pathorName);
        } catch (IOException e) {
            log.error("本地下载失败",e);
        }
    }

    @Override
    public void downloadZip(String pathOrName, HttpServletResponse response, Map<String, Map<String, String>> dataMap) {
        if (MapUtil.isNotEmpty(dataMap)){
            try {
                File zip = new File(pathOrName);
                OutputStream outputStream = new FileOutputStream(zip);
                ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);
                Map<String, String> mdMap = dataMap.get(DownConstant.MD_DATA_MAP);
                if (MapUtil.isNotEmpty(mdMap)) {
                    WebMagicUtil.writeZosMd(mdMap,zipOutputStream);
                }
                Map<String, String> imgMap = dataMap.get(DownConstant.IMG_DATA_MAP);
                if (MapUtil.isNotEmpty(imgMap)) {
                    Map<String, InputStream> inputStreamMap = WebMagicUtil.inputStreamMap(imgMap);
                    int size = inputStreamMap.size();
                    String[] keys = new ArrayList<>(inputStreamMap.keySet()).toArray(new String[size]);
                    InputStream[] values = new ArrayList<>(inputStreamMap.values()).toArray(new InputStream[size]);
                    ZipUtil.zip(outputStream,keys,values);
                }
                IoUtil.close(zipOutputStream);
                IoUtil.close(outputStream);
                log.info("\n当前下载文件到本地->{}完成。",pathOrName);
            } catch (FileNotFoundException e) {
                log.error("本地下载失败",e);
            }
        }
    }

    @Override
    public void downloadZip( ZipOutputStream zos, Map<String, Map<String, String>> dataMap) {
      WebMagicUtil.writeZosZip2(zos,dataMap);
    }
}
