package com.blacktea.webmagic.demo.domain;

import lombok.Data;

import javax.servlet.http.HttpServletResponse;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/26 13:56
 */
@Data
public class BrowseDownload extends Download{

    /** 浏览器下载时用于输出的流,单个节点(使用{@link ItemPage})时必传 **/
    private HttpServletResponse response;
}
