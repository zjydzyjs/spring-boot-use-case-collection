package com.blacktea.webmagic.demo.processor;

import lombok.Data;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

import java.util.function.Consumer;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/27 8:43
 */
@Data
public abstract class AbstractPageProcessor implements PageProcessor {

    private final Site site;
    private final Consumer<Page> pageConsumer;

    @Override
    public void process(Page page) {
        pageConsumer.accept(page);
    }

    @Override
    public Site getSite() {
        return site;
    }


}
