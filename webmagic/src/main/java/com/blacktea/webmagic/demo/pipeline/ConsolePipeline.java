package com.blacktea.webmagic.demo.pipeline;

import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import us.codecraft.webmagic.ResultItems;

import java.util.Map;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/27 10:46
 */
@Slf4j
public class ConsolePipeline extends AbstractDownloadPipeline{
    @Override
    public void download(ResultItems resultItems) {
        Map<String, Object> all = resultItems.getAll();
        if (MapUtil.isEmpty(all)){
            return;
        }
        all.forEach((k,v) -> {
            log.debug("当前匹配-key:{},value:{}",k, JSON.toJSONString(v));
        });
    }
}
