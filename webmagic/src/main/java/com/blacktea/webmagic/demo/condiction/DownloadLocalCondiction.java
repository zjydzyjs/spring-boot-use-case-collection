package com.blacktea.webmagic.demo.condiction;

import cn.hutool.core.lang.Assert;
import com.blacktea.webmagic.demo.domain.LocalDownload;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/26 13:53
 */
public class DownloadLocalCondiction extends BaseDownloadCondiction<LocalDownload>{

    @Override
    public void check(LocalDownload download) {
        super.check(download);
        Assert.notBlank(download.getPath());
    }
}
