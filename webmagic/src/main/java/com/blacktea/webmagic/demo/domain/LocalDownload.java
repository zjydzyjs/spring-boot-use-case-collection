package com.blacktea.webmagic.demo.domain;

import lombok.Data;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/26 13:54
 */
@Data
public class LocalDownload extends Download{

    /** 地址前缀,当下载到本地,必须要该值,例如:{@link com.blacktea.webmagic.demo.constant.DownConstant.LocalDown} DEFAULE_PATH **/
    private String path;
}
