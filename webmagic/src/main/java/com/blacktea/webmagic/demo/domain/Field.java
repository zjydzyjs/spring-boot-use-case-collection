package com.blacktea.webmagic.demo.domain;

import com.blacktea.webmagic.demo.constant.FileTypeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import us.codecraft.webmagic.model.annotation.ExtractBy;

/**
 * @description: 0.0.1 - version支持 单一type 处理,暂不支持 多重type 组合处理
 * @author: black tea
 * @date: 2021/9/28 10:10
 */
@Data
@ApiModel(description = "字段条件")
public class Field {

    @ApiModelProperty(value = "匹配条件唯一名称(不要重合,重合后会随机取其一),下载zip时会作为父级目录。",required = true,example = "itemMd")
    private String key;

    @ApiModelProperty(value = "匹配条件内容值,与type一同使用",required = true,example = "//*[@id=\"article_content\"]/[@id=\"content_views\"")
    private String value;

    @ApiModelProperty(value = "匹配条件内容类型,例如XPath(类型:type)",required = true,example = "XPath")
    private ExtractBy.Type type;

    @ApiModelProperty(value = "匹配条件内容文件类型,例如MD(类型:fileType)",required = true,example = "MD")
    private FileTypeConstant.FileTypeEnum fileType;

}
