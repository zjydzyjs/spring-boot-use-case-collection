package com.blacktea.webmagic.demo.condiction;

import cn.hutool.core.lang.Assert;
import com.blacktea.webmagic.demo.domain.Download;
import com.blacktea.webmagic.demo.domain.Field;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/26 14:00
 */
public class BaseDownloadCondiction<T extends Download> implements Condiction<T>{

    @Override
    public void check(T t) {
        Download download = t;
        Assert.notNull(download);
        Assert.notNull(download.getPage());
        List<Field> fields = download.getPage().getFields();
        if (CollectionUtils.isEmpty(fields)){
            Assert.state(false,"字段条件[fields]不可以为空!");
        }
        List<Field> collect = fields.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(Field::getKey))), ArrayList::new));
        download.getPage().setFields(collect);
        Assert.notNull(download.getPage().getFileName());
    }
}
