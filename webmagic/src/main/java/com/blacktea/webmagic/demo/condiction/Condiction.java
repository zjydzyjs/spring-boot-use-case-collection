package com.blacktea.webmagic.demo.condiction;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/26 13:51
 */
public interface Condiction<T> {

    /**
     * 校验参数 t 是否满足
     * @param t
     */
    void check(T t);
}
