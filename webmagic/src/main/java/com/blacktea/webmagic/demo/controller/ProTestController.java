package com.blacktea.webmagic.demo.controller;

import com.blacktea.webmagic.demo.domain.ItemPage;
import com.blacktea.webmagic.demo.domain.ListPage;
import com.blacktea.webmagic.demo.service.ExportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/24 16:18
 */
@RestController
@RequestMapping("pro")
@RequiredArgsConstructor
@Api(tags = "ProTestController", value = "测试导出")
public class ProTestController {

    private final ExportService exportService;

    @ApiOperation(value = "导出文章md")
    @PostMapping("item/export/md")
    public void exportCSDNMd(@RequestBody ItemPage itemPage, HttpServletResponse response){
        exportService.itemExportMd(itemPage,response);
    }

    @ApiOperation(value = "导出文章匹配内容zip")
    @PostMapping("item/export/zip")
    public void exportCSDNZip(@RequestBody ItemPage itemPage,HttpServletResponse response){
        exportService.itemExportZip(itemPage,response);
    }

    @ApiOperation(value = "根据列表页导出目标文章匹配内容zip")
    @PostMapping("list/export/zip")
    public void exportListZip(@RequestBody ListPage listPage, HttpServletResponse response){
        exportService.listExportZip(listPage,response);
    }
}
