package com.blacktea.webmagic.demo.processor;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;

import java.util.function.Consumer;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/27 8:48
 */
public class CSDNListProcessor extends AbstractPageProcessor {

    public CSDNListProcessor(Site site, Consumer<Page> pageConsumer) {
        super(site, pageConsumer);
    }

}
