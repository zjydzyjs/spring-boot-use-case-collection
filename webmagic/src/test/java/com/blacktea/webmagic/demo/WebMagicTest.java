package com.blacktea.webmagic.demo;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import com.blacktea.webmagic.demo.config.MyHttpClientDownloader;
import com.blacktea.webmagic.demo.constant.DownConstant;
import com.blacktea.webmagic.demo.constant.FileTypeConstant;
import com.blacktea.webmagic.demo.domain.Field;
import com.blacktea.webmagic.demo.domain.ItemPage;
import com.blacktea.webmagic.demo.domain.ListPage;
import com.blacktea.webmagic.demo.domain.LocalDownload;
import com.blacktea.webmagic.demo.pipeline.ItemDownloadPipeline;
import com.blacktea.webmagic.demo.pipeline.ListDownloadPipeline;
import com.blacktea.webmagic.demo.pipeline.MDDownloadPipeline;
import com.blacktea.webmagic.demo.processor.CSDNItemArticleProcessor;
import com.blacktea.webmagic.demo.processor.CSDNListProcessor;
import com.blacktea.webmagic.demo.util.WebMagicUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.model.annotation.ExtractBy;
import us.codecraft.webmagic.processor.example.GithubRepoPageProcessor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipOutputStream;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/26 15:42
 */
@Slf4j
public class WebMagicTest {

    @Test
    void testDemo1(){
        // 出现错误 -> javax.net.ssl.SSLHandshakeException: Received fatal alert: protocol_version
//        Spider.create(new GithubRepoPageProcessor())
//                .addUrl(new String[]{"https://github.com/code4craft"})
//                .thread(5).run();

        //使用MyHttpClientDownloader解决  protocol_version
        Spider.create(new GithubRepoPageProcessor())
                .addUrl(new String[]{"https://github.com/code4craft"})
                .setDownloader(new MyHttpClientDownloader())
                .thread(5).run();
    }


    @Test
    void downloadItem(){
        boolean isZipDown = true;

        LocalDownload download = new LocalDownload();
        download.setPath(DownConstant.LocalDown.DEFAULE_PATH);
        ItemPage itemPage = new ItemPage();
        itemPage.setItemPageUrl("https://blog.csdn.net/weixin_43917143/article/details/120436445");
        download.setPage(itemPage);

        if (!isZipDown){
            // md下载
            download.getPage().setFileName(DownConstant.DEFAULT_MD.fileName());
            Field field1 = new Field();
            field1.setFileType(FileTypeConstant.FileTypeEnum.MD);
            field1.setType(ExtractBy.Type.XPath);
            field1.setKey("md1");
            field1.setValue("//*[@id=\"article_content\"]/[@id=\"content_views\"]");
            // 当需要多个文本元素,每个html-文本元素都形成md(标签内容)进行拼接 -> 最后将md写入一个md文件
            Field field2 = new Field();
            field2.setFileType(FileTypeConstant.FileTypeEnum.MD);
            field2.setType(ExtractBy.Type.XPath);
            field2.setKey("md2");
            field2.setValue("//*[@id=\"article_content\"]/[@id=\"content_views\"");
            itemPage.setFields(Arrays.asList(field1,field2));
        }else {
            // zip下载
            download.getPage().setFileName(DownConstant.DEFAULT_ZIP.fileName());
            Field field1 = new Field();
            field1.setFileType(FileTypeConstant.FileTypeEnum.PNG);
            field1.setType(ExtractBy.Type.XPath);
            field1.setKey("imgs");
            field1.setValue("//*[@id=\"article_content\"]//*//img");
            // 当存储的field有多个时,会将所有匹配到的按field.type类型进行匹配储存
            Field field2 = new Field();
            field2.setFileType(FileTypeConstant.FileTypeEnum.PNG);
            field2.setType(ExtractBy.Type.XPath);
            field2.setKey("icons");
            field2.setValue("//div[@class=\"mouse-box\"]//img");
            Field field3 = new Field();
            field3.setFileType(FileTypeConstant.FileTypeEnum.MD);
            field3.setType(ExtractBy.Type.XPath);
            field3.setKey("mds");
            field3.setValue("//*[@id=\"article_content\"]/[@id=\"content_views\"");
            itemPage.setFields(Arrays.asList(field1));
        }
        CSDNItemArticleProcessor csdnItemArticleProcessor = (CSDNItemArticleProcessor) WebMagicUtil.csdnProcess(download,isZipDown);
        Spider spider = Spider.create(csdnItemArticleProcessor)
                .setDownloader(new MyHttpClientDownloader())
                .addUrl(itemPage.getItemPageUrl())
                .thread(5);
        if (!isZipDown){
            spider.addPipeline(MDDownloadPipeline.download(download));
        }else {
            spider.addPipeline(ItemDownloadPipeline.download(download));
        }
        // 启动,只能是run,@Test和Main不一样
        spider.run();
    }

    @Test
    void downloadList(){
        LocalDownload download = new LocalDownload();
        download.setPath(DownConstant.LocalDown.DEFAULE_PATH);
        ListPage listPage = new ListPage();
        listPage.setHelpUrl("https://blog.csdn.net/weixin_43917143");
        listPage.setTargetUrl("(https://blog\\.csdn\\.net/weixin_43917143/article/details/\\w+)");
        download.setPage(listPage);
        // 设置fields
        Field field1 = new Field();
        field1.setFileType(FileTypeConstant.FileTypeEnum.MD);
        field1.setType(ExtractBy.Type.XPath);
        field1.setKey("文章1md");
        field1.setValue("//*[@id=\"article_content\"]/[@id=\"content_views\"");
        Field field2 = new Field();
        field2.setFileType(FileTypeConstant.FileTypeEnum.MD);
        field2.setType(ExtractBy.Type.XPath);
        field2.setKey("文章2md");
        field2.setValue("//*[@id=\"article_content\"]/[@id=\"content_views\"");
        Field field3 = new Field();
        field3.setFileType(FileTypeConstant.FileTypeEnum.PNG);
        field3.setType(ExtractBy.Type.XPath);
        field3.setKey("icons");
        field3.setValue("//div[@class=\"mouse-box\"]//img");

        List<Field> fields = Arrays.asList(field1,field2,field3);
        listPage.setFields(fields);
        CSDNListProcessor csdnListProcessor = (CSDNListProcessor) WebMagicUtil.csdnProcess(download, true);

        try {
            // 存储位置
            String fileName = DownConstant.LocalDown.DEFAULE_PATH + "\\列表" + DownConstant.DEFAULT_ZIP.getDefaultFileType();
            listPage.setFileName(fileName);
            boolean exist = FileUtil.exist(fileName);
            if (!exist){
                FileUtil.mkParentDirs(fileName);
            }
            FileOutputStream fileOutputStream = new FileOutputStream(fileName);
            ZipOutputStream zos = new ZipOutputStream(fileOutputStream);
            download.setZipOutputStream(zos);
            ListDownloadPipeline listDownloadPipeline = new ListDownloadPipeline(download);
            WebMagicUtil.create(csdnListProcessor,listPage.getHelpUrl(),listDownloadPipeline).run();
            IoUtil.close(zos);
            IoUtil.close(fileOutputStream);

        } catch (IOException e) {
            log.error("列表页找寻目标页面内容失败,出现异常!",e);
        }
    }
}
