package com.blacktea.es.dao.repository;

import com.blacktea.es.entites.dto.DemoEsDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/10 15:24
 */
@Repository
public interface DemoElasticsearchRepository extends ElasticsearchRepository<DemoEsDTO,String> {

    List<DemoEsDTO> getByNumber(Integer number);

    List<DemoEsDTO> getByDes(String des);

    void deleteByDes(String des);

}
