package com.blacktea.es.dao.es;

import com.blacktea.es.entites.domain.Document;
import com.xpc.easyes.core.conditions.interfaces.BaseEsMapper;

/**
 * @description:
 * @author: black tea
 * @date: 2022/3/15 16:11
 */
public interface DocumentMapper extends BaseEsMapper<Document> {
}
