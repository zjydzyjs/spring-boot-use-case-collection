package com.blacktea.es.entites.dto;

import lombok.Data;
import org.elasticsearch.search.sort.SortOrder;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/7 10:00
 */
@Data
public class ESSearchDto implements Serializable {
    private static final long serialVersionUID = -4415358090181890725L;

    private List<ESConditionDTO> conditionDTOS;

    private Map<String, SortOrder> sortOrderMap;

    public ESSearchDto(List<ESConditionDTO> conditionDTOS) {
        this.conditionDTOS = conditionDTOS;
    }

    public ESSearchDto(List<ESConditionDTO> conditionDTOS, Map<String, SortOrder> sortOrderMap) {
        this.conditionDTOS = conditionDTOS;
        this.sortOrderMap = sortOrderMap;
    }
}
