package com.blacktea.es.entites.vo;

import com.blacktea.es.entites.dto.DemoEsDTO;
import lombok.Data;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/9 13:58
 */
@Data
public class DemoEsVo extends DemoEsDTO {

    /** 上一次查询最后一个文档的sort,用于下也一个查询,只用于 {@link com.blacktea.es.entites.dto.RequestSearchAfterPage} **/
    private Object[] sort;

}
