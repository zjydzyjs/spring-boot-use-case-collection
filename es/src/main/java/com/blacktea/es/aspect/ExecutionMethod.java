package com.blacktea.es.aspect;

import com.sun.istack.internal.NotNull;

import java.lang.annotation.*;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/8 16:04
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExecutionMethod {

    @NotNull
    String name();

    String des() default "";
}
