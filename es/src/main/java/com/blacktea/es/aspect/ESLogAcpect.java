package com.blacktea.es.aspect;

import com.blacktea.es.entites.common.ESConst;
import com.blacktea.es.util.ESUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/8 16:11
 */
@Component
@Aspect
@Slf4j
public class ESLogAcpect {

    @Value("${spring.elasticsearch.log.level}")
    private int level;

    /**
     * 定义切入点
     */
    @Pointcut("@annotation(com.blacktea.es.aspect.ExecutionMethod)")
    public void esLog(){}

    /**
     * 前置通知：在连接点之前执行的通知
     * @param joinPoint
     * @throws Throwable
     */
    @Before("esLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        ExecutionMethod annotation = signature.getMethod().getAnnotation(ExecutionMethod.class);
        ESUtil.esOperationLog(ESConst.ESLogLevelEnum.getByLevel(level),annotation,"开始");
    }

    @After(value = "esLog()")
    public void doAfterReturning(JoinPoint  joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        ExecutionMethod annotation = signature.getMethod().getAnnotation(ExecutionMethod.class);
        ESUtil.esOperationLog(ESConst.ESLogLevelEnum.getByLevel(level),annotation,"结束");
    }
}
