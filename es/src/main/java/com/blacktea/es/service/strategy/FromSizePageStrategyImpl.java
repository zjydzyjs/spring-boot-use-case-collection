package com.blacktea.es.service.strategy;

import com.blacktea.es.entites.common.ESConst;
import com.blacktea.es.entites.dto.PageRequest;
import com.blacktea.es.util.ESUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/7 10:54
 */
@Service("fromSizePageStrategy")
@RequiredArgsConstructor
@Slf4j
public class FromSizePageStrategyImpl<T> implements ESRequestPageStrategy<T>{

    private final RestHighLevelClient restHighLevelClient;
    @Value("${spring.elasticsearch.log.level}")
    private int level;

    @Override
    public List<T> list(Class<T> var1, SearchRequest searchRequest, PageRequest pageRequest) throws IOException {
        SearchSourceBuilder searchSourceBuilder = searchRequest.source();
        searchSourceBuilder.from(pageRequest.getPage());
        searchSourceBuilder.size(pageRequest.getLimit());
        ESUtil.esLog(ESConst.ESLogLevelEnum.getByLevel(level),searchRequest);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        return ESUtil.searchResponseToList(searchResponse, var1);
    }
}
