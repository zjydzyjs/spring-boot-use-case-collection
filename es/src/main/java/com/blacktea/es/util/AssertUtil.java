package com.blacktea.es.util;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/8 8:55
 */
public class AssertUtil {

    public static void fail(String message) {
        throw new ESException(message);
    }

    public static void fail(IErrorCode errorCode) {
        throw new ESException(errorCode);
    }

}
