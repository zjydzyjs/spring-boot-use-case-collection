package com.blacktea.es.util;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/8 8:59
 */
public interface IErrorCode {

    long getCode();

    String getMessage();
}
