package com.blacktea.es.util;

/**
 * @description:
 * @author: black tea
 * @date: 2021/9/8 8:54
 */
public class ESException extends RuntimeException{

    private IErrorCode errorCode;
    public ESException(String message) {
        super(message);
    }

    public ESException(IErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public IErrorCode getErrorCode() {
        return errorCode;
    }
}
