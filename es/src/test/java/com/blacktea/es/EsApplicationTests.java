package com.blacktea.es;

import com.alibaba.fastjson.JSON;
import com.blacktea.es.entites.common.ESConst;
import com.blacktea.es.entites.dto.*;
import com.blacktea.es.entites.vo.DemoEsVo;
import com.blacktea.es.service.ElasticsearchService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.*;

@SpringBootTest
@Slf4j
class EsApplicationTests {

    @Autowired
    private ElasticsearchService elasticsearchService;

    @Test
    void getById() throws IOException {
        DemoEsDTO demo = (DemoEsDTO)elasticsearchService.getDocument(DemoEsDTO.class, "demo", "1");
        log.info("result:{}",JSON.toJSONString(demo));
    }

    @Test
    void createOne() throws IOException {
        DemoEsDTO demoEsDTO = new DemoEsDTO(22L, "名称"+22, "测试添加(create)", Math.toIntExact(10 + 22));
        DemoEsDTO demo = (DemoEsDTO) elasticsearchService.createDocument(DemoEsDTO.class, demoEsDTO, "demo", String.valueOf(demoEsDTO.getId()));
        log.info("添加结果:{}",JSON.toJSONString(demo));
    }

    @Test
    void deleteById() throws IOException {
        boolean del = elasticsearchService.deleteDocument("demo", String.valueOf(22L));
        log.info("删除成功:{}",del);
    }

    /**
     * 根据单个条件删除(Term 相等条件)
     */
    @Test
    void deleteByOne() throws IOException {
        String key = "des.keyword";
        Object val = "测试添加";
        Long delResult = elasticsearchService.deleteDocument("demo", key, val);
        log.info("根据单条件删除文档数量:{}",delResult);
    }

    /**
     * 根据多个条件删除(条件自定义)
     */
    @Test
    void deleteDocumentByCondition() throws IOException {
        List<ESConditionDTO> conditionDTOS = new ArrayList<>();
        ESConditionDTO dto1 = ESConditionDTO.builder()
                .and(false)
                .k("name.keyword")
                .v("名称11")
                .operation(ESConst.ESOperationEnum.TERM_QUERY)
                .build();
        conditionDTOS.add(dto1);
        ESConditionDTO dto2 = ESConditionDTO.builder()
                .and(false)
                .k("des.keyword")
                .v("测试修改1")
                .operation(ESConst.ESOperationEnum.TERM_QUERY)
                .build();
        conditionDTOS.add(dto2);
        // 删除name=名称11 or des=测试修改1
        log.info("最终删除数量:{}",elasticsearchService.deleteDocumentByCondition("demo", conditionDTOS));

    }

    /**
     * 根据 _id 集合进行批量删除
     */
    @Test
    void deleteBatchDocument() throws IOException {
        List<String> ids = Arrays.asList("12","13","14");
        log.info("根据_id集合批量删除成功:{}",elasticsearchService.deleteBatchDocument("demo",ids));
    }

    @Test
    void updateById() throws IOException {
        DemoEsDTO demo = (DemoEsDTO)elasticsearchService.getDocument(DemoEsDTO.class, "demo", "3");
        log.info("原数据:{}",JSON.toJSONString(demo));
        demo.setDes("测试修改3");
        DemoEsDTO updateDemo = (DemoEsDTO) elasticsearchService.updateDocument(DemoEsDTO.class, demo, "demo", String.valueOf(demo.getId()));
        log.info("修改后的数据:{}",JSON.toJSONString(updateDemo));
    }

    /**
     * 根据条件修改(条件自定义)
     */
    @Test
    void updateDocumentByCondition() throws IOException {
        // 修改后的对象
        DemoEsDTO demoEsDTO = new DemoEsDTO();
        demoEsDTO.setDes("批按条件修改script");
        demoEsDTO.setName("名称+修改script");
        demoEsDTO.setNumber(0);
        // 条件 id=4 or id =5 预计结果: 4和5都会被修改,修改数目为2
        ESConditionDTO dto1 = ESConditionDTO.builder()
                .and(false)
                .k("id")
                .v("4")
                .operation(ESConst.ESOperationEnum.TERM_QUERY)
                .build();
        ESConditionDTO dto2 = ESConditionDTO.builder()
                .and(false)
                .k("id")
                .v("5")
                .operation(ESConst.ESOperationEnum.TERM_QUERY)
                .build();
        List<ESConditionDTO> esConditionDTOS = Arrays.asList(dto1,dto2);
        long updateResult = elasticsearchService.updateDocumentByCondition(demoEsDTO, "demo", esConditionDTOS);
        log.info("按条件修改结果数目:{}",updateResult);
    }

    /**
     * 批量修改 - 根据id
     */
    @Test
    void updateBatchDocument() throws IOException {
        Map<String,Object> params = new HashMap<>();
        DemoEsDTO demoEsDTO1 = new DemoEsDTO();
        demoEsDTO1.setDes("按id批量修改16");
        demoEsDTO1.setName("名称-按id批量修改");
        params.put("16",demoEsDTO1);
        DemoEsDTO demoEsDTO2 = new DemoEsDTO();
        demoEsDTO2.setName("名称-按id批量修改");
        demoEsDTO2.setDes("按id批量修改17");
        params.put("17",demoEsDTO2);
       log.info("根据id,批量修改结果:{}",elasticsearchService.updateBatchDocument("demo",params));
    }

    @Test
    void addBatch() throws IOException {
        Map<String,Object> map = new HashMap<>();
        for (long i = 1L; i <= 21L; i++) {
            DemoEsDTO demoEsDTO = new DemoEsDTO(i, "名称"+i, "测试添加", Math.toIntExact(10 + i));
            map.put(String.valueOf(i),demoEsDTO);
        }
        log.info("批量添加文档结果:{}",elasticsearchService.addBatchDocument(map,"demo"));
    }

    @Test
    void getListByAndMap() throws IOException {
        Map<String,Object> map = new HashMap<>();
        // 当des是keyword时,k -> des.keyword term才可以匹配到
        map.put("des.keyword","测试添加");
        // 不加sort
//        List list = elasticsearchService.getListByAndMap(DemoEsDTO.class, "demo", map);
        // 带有sort
        HashMap<String, SortOrder> sortOrderHashMap = new HashMap<>();
        // 当des是keyword时,k -> des.keyword ,这样就不会报错
        sortOrderHashMap.put("id",SortOrder.DESC);
        List<DemoEsDTO> list =  elasticsearchService.getListByAndMap(DemoEsDTO.class, "demo", map,sortOrderHashMap);
        log.info("通过(map)全匹配结果,size:{},list:{}",list.size(),JSON.toJSONString(list));
    }

    @Test
    void getListCondition() throws IOException {

        // or 语句 假设我要查询 name = 名称2 or name = 名称1
        List<ESConditionDTO> conditionDos = new ArrayList<>();
        ESConditionDTO build1 = ESConditionDTO.builder()
                .k("name.keyword")
                .v("名称2")
                .and(false)
                .operation(ESConst.ESOperationEnum.TERM_QUERY)
                .build();
        conditionDos.add(build1);
        ESConditionDTO build2 = ESConditionDTO.builder()
                .k("name.keyword")
                .v("名称1")
                .and(false)
                .operation(ESConst.ESOperationEnum.TERM_QUERY)
                .build();
        conditionDos.add(build2);

//        // and 语句 假设我要查 des = 测试添加 && name = 名称2
//        conditionDos.clear();
//        ESConditionDTO build3 = ESConditionDTO.builder()
//                .k("des.keyword")
//                .v("测试添加")
//                .and(true)
//                .operation(ESConst.ESOperationEnum.TERM_QUERY)
//                .build();
//        conditionDos.add(build3);
//        ESConditionDTO build4 = ESConditionDTO.builder()
//                .k("name.keyword")
//                .v("名称2")
//                .and(true)
//                .operation(ESConst.ESOperationEnum.TERM_QUERY)
//                .build();
//        conditionDos.add(build4);
//
//        // and or 组合 总结果: and的结果就是总结果
//        // 1: 假设我要查询 and des = 测试添加,or name = 名称1
//        // 总结: and 查出的结果就是总结果
//        conditionDos.clear();
//        ESConditionDTO build5 = ESConditionDTO.builder()
//                .k("des.keyword")
//                .v("测试添加")
//                .and(true)
//                .operation(ESConst.ESOperationEnum.TERM_QUERY)
//                .build();
//        conditionDos.add(build5);
//        ESConditionDTO build6 = ESConditionDTO.builder()
//                .k("name.keyword")
//                .v("名称1")
//                .and(false)
//                .operation(ESConst.ESOperationEnum.TERM_QUERY)
//                .build();
//        conditionDos.add(build6);
//        // 2: 假设我要查询 and name = 名称1,or name = 名称2
//        // 总结: and 查出的结果就是总结果
//        conditionDos.clear();
//        ESConditionDTO build7 = ESConditionDTO.builder()
//                .k("name.keyword")
//                .v("名称1")
//                .and(true)
//                .operation(ESConst.ESOperationEnum.TERM_QUERY)
//                .build();
//        conditionDos.add(build7);
//        ESConditionDTO build8 = ESConditionDTO.builder()
//                .k("name.keyword")
//                .v("名称2")
//                .and(false)
//                .operation(ESConst.ESOperationEnum.TERM_QUERY)
//                .build();
//        conditionDos.add(build8);

        // 不带排序
//        List list = elasticsearchService.getListByCondition(DemoEsDTO.class, "demo", conditionDos);
        // 带排序
        ESSearchDto esSearchDto = new ESSearchDto(conditionDos);
        HashMap<String, SortOrder> sortOrderHashMap = new HashMap<>();
        sortOrderHashMap.put("id",SortOrder.ASC);
        esSearchDto.setSortOrderMap(sortOrderHashMap);
        List list = elasticsearchService.getListByCondition(DemoEsDTO.class, "demo", esSearchDto);
        log.info("通过自定义条件DTO查询到的结果,size:{},list:{}",list.size(),JSON.toJSONString(list));
    }


    /**
     *
     * ps -> es 分页需知 https://www.jianshu.com/p/733e7e1e4de5
     * 这里感谢 {小胖学编程} https://www.jianshu.com/u/106c065a14f4
     * 分页很多都有借鉴他的思想。
     *
     * 测试三种分页分页,三种分页共同搜索条件
     */
    private ESSearchDto getSearchDto(){
        List<ESConditionDTO> conditionDTOS = new ArrayList<>();
        ESConditionDTO build5 = ESConditionDTO.builder()
                .k("des.keyword")
                .v("测试添加")
                .and(true)
                .operation(ESConst.ESOperationEnum.TERM_QUERY)
                .build();
        conditionDTOS.add(build5);
        Map<String, SortOrder> sortOrderMap = new HashMap<>();
        sortOrderMap.put("id",SortOrder.ASC);
        return new ESSearchDto(conditionDTOS,sortOrderMap);
    }

    /**
     * FromSize -> 指定跳转分页 page=0表示从第一页开始,page=10表示第二页,一次类推,每加一页 page=page+10
     */
    @Test
    void getFromSizePage() throws IOException {
        // 指定跳转分页 - from-size
        ESSearchDto esSearchDto = this.getSearchDto();
        int page = 0;
        RequestFromSizePage requestFromSizePage = RequestFromSizePage.of(page, 10);
        Page<DemoEsDTO> pageFromSizeByCondition = elasticsearchService.getPageFromSizeByCondition(DemoEsDTO.class,"demo",esSearchDto,requestFromSizePage);
        do {
            log.info("本次分页参数:{},结果,size:{},page:{}",JSON.toJSONString(requestFromSizePage),pageFromSizeByCondition.getContent().size(),JSON.toJSONString(pageFromSizeByCondition));
            requestFromSizePage.setPage(requestFromSizePage.getPage()+10);
            pageFromSizeByCondition = elasticsearchService.getPageFromSizeByCondition(DemoEsDTO.class,"demo",esSearchDto,requestFromSizePage);
        }while (!CollectionUtils.isEmpty(pageFromSizeByCondition.getContent()));
    }

    /**
     * Scroll -> 查询满足条件的所有数据, 实际上该分页已不可以分页,查出的内容为全部
     *
     *   注意: 我当前已经直接查询所有数据了,为了满足list all,如果需要实现分页的效果,
     *   1： 请自己重写 {@link com.blacktea.es.service.strategy.ESRequestPageStrategy}
     *   2： 请修改 {@link ElasticsearchService} 下的 list()
     *   3:  入参时,scrollTimeValue 必须设置的时间长一点,不然快照就过期了,但是这样会导致空间被大量占用
     */
    @Test
    void getScrollPage() throws IOException {
        ESSearchDto esSearchDto = this.getSearchDto();
        RequestScrollPage requestScrollPage = RequestScrollPage.of(10, TimeValue.timeValueMinutes(2));
        Page<DemoEsDTO> page = elasticsearchService.getPageByCondition(DemoEsDTO.class, "demo", esSearchDto, requestScrollPage);
        log.info("本次分页结果,size:{},page:{}",page.getContent().size(),JSON.toJSONString(page));
    }

    /**
     * SearchAfter ->
     *   该方法只建议用于滚动的场景,不可以指定跳转页数
     *   例如app的下翻,只能回到顶部那种,回到顶部不要传 Object[] values,就会直接默认查询从 page=0 ~ limit,
     *   该方法只能指定page=0(es 默认page=0，limit=10),所以你传page也没用,我不会设置该参数,
     *   如果想要实现首页的跳转,那么你可以你自己实现传输page试试! 我没试过!
     *
     *  用例解析: 也就是我为什么要怎么写?
     *
     *   RequestSearchAfterPage.of(10, "name.keyword", SortOrder.ASC,null)
     *   // 当 values 为null时,表示你是要进行首页查询(page=0,limit)
     *
     *   while (!CollectionUtils.isEmpty(page.getContent()));
     *   // 表示下一页内容是否没有了,也就是页面上的到底了,一滴都没有了,大兄弟,没有了
     *
     *   elasticsearchService.getPageByCondition(DemoEsVo.class, "demo", esSearchDto, requestSearchAfterPage);
     *   // 为什么我的文档类是 {@link DemoEsDTO} 用的却是 {@link DemoEsVo},这是因为SearchAfter的特殊性,当你要查询下一页时
     *   你必须的有上页某个文档的sort属性( 也就是这里的{@link DemoEsVo} -> values ),你得把它返回给用户,在他要往下翻页的时候
     *   再传回来入参 {@link com.blacktea.es.service.strategy.SearchAfterPageStrategyImpl}
     *   里的 -> {searchSourceBuilder.searchAfter(values);}
     *   作为 search_after 的入参
     *
     */
    @Test
    void getSearchAfterPage() throws IOException {
        ESSearchDto esSearchDto = this.getSearchDto();
        RequestSearchAfterPage requestSearchAfterPage = RequestSearchAfterPage.of(10, "name.keyword", SortOrder.ASC,null);
        Page<DemoEsVo> page = elasticsearchService.getPageByCondition(DemoEsVo.class, "demo", esSearchDto, requestSearchAfterPage);
        do {
            log.info("本次分页结果,size:{},page:{}",page.getContent().size(),JSON.toJSONString(page));
            Object[] sort = page.getContent().get(page.getContent().size() - 1).getSort();
            requestSearchAfterPage.setValues(sort);
            page = elasticsearchService.getPageByCondition(DemoEsVo.class, "demo", esSearchDto, requestSearchAfterPage);
        }while (!CollectionUtils.isEmpty(page.getContent()));
    }

}
