package com.blacktea.es;

import com.alibaba.fastjson.JSON;
import com.blacktea.es.dao.es.DocumentMapper;
import com.blacktea.es.entites.domain.Document;
import com.xpc.easyes.core.conditions.LambdaEsIndexWrapper;
import com.xpc.easyes.core.conditions.LambdaEsQueryWrapper;
import com.xpc.easyes.core.enums.FieldType;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
@Slf4j
class EsEasyApplicationTests {

    @Autowired
    DocumentMapper documentMapper;

    @Test
    public void testCreatIndex() {
        // 测试创建索引 不了解Es索引概念的建议先去了解 懒汉可以简单理解为MySQL中的一张表
        LambdaEsIndexWrapper<Document> wrapper = new LambdaEsIndexWrapper<>();

        // 此处简单起见 直接使用类名作为索引名称 后面章节会教大家更如何灵活配置和使用索引
        wrapper.indexName(Document.class.getSimpleName().toLowerCase());

        // 此处将文章标题映射为keyword类型(不支持分词),文档内容映射为text类型(支持分词查询)
        wrapper.mapping(Document::getTitle, FieldType.KEYWORD)
                .mapping(Document::getContent, FieldType.TEXT);

        boolean isOk = documentMapper.createIndex(wrapper);
//        Boolean isOk = documentMapper.deleteIndex(Document.class.getSimpleName().toLowerCase());
//        System.out.println(isOk);
        // 期望值: true 如果是true 则证明索引已成功创建
    }

    @Test
    public void testGetById(){
        LambdaEsQueryWrapper<Document> wrapper = new LambdaEsQueryWrapper<>();
//        wrapper.eq(Document::getTitle,"title");
        List<Document> documents = documentMapper.selectList(wrapper);
        System.out.println(JSON.toJSONString(documents));


    }

    @Test
    public void testSave(){
        Document document = new Document();
        document.setTitle("title");
        System.out.println(documentMapper.insert(document));
    }

}
