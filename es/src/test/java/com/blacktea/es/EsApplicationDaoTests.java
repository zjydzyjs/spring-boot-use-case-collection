package com.blacktea.es;

import com.alibaba.fastjson.JSON;
import com.blacktea.es.dao.repository.Demo2ElasticsearchRepository;
import com.blacktea.es.dao.repository.DemoElasticsearchRepository;
import com.blacktea.es.entites.dto.Demo2EsDTO;
import com.blacktea.es.entites.dto.DemoEsDTO;
import com.blacktea.es.entites.dto.UserEsDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@Slf4j
class EsApplicationDaoTests {

    @Autowired
    private DemoElasticsearchRepository demoElasticsearchRepository;
    @Autowired
    private Demo2ElasticsearchRepository demo2ElasticsearchRepository;

    @Test
    void getById() throws IOException {
        Optional<DemoEsDTO> demoEsDTO = demoElasticsearchRepository.findById("17");
        log.info("byId:{}",demoEsDTO.orElseGet(DemoEsDTO::new));
    }

    @Test
    void getByDes(){
        List<DemoEsDTO> dtos = demoElasticsearchRepository.getByDes("测试添加");
        log.info("size:{},list:{}",dtos.size(),JSON.toJSONString(dtos));
    }

    @Test
    void getByNumber(){
        List<DemoEsDTO> dtos = demoElasticsearchRepository.getByNumber(26);
        log.info("size:{},list:{}",dtos.size(),JSON.toJSONString(dtos));
    }

    @Test
    void findAll(){
        PageImpl<DemoEsDTO> all = (PageImpl) demoElasticsearchRepository.findAll();
        log.info("size:{},page:{}",all.getContent().size(),JSON.toJSONString(all));
        Iterable<DemoEsDTO> all1 = demoElasticsearchRepository.findAll();
        log.info("page:{}",JSON.toJSONString(all1));
    }

    @Test
    void findAll2(){
        PageImpl<Demo2EsDTO> all = (PageImpl) demo2ElasticsearchRepository.findAll();
        log.info("size:{},page:{}",all.getContent().size(),JSON.toJSONString(all));
        Iterable<Demo2EsDTO> all1 = demo2ElasticsearchRepository.findAll();
        log.info("page:{}",JSON.toJSONString(all1));
    }

    /**
     * 测试后可知,
     *   当 _id 已存在后,相同 _id 会覆盖,
     *   不存在 _id会自动新增;
     */
    @Test
    void saveAll(){
        List<Demo2EsDTO> demo2EsDTOS = new ArrayList<>();
        for (long i = 23L; i <= 25L; i++) {
            UserEsDTO userEsDTO = new UserEsDTO("user"+i, Math.toIntExact(i));
            Demo2EsDTO demoEsDTO = new Demo2EsDTO(i, "Repository名称"+i, "测试添加Repository-saveAll", userEsDTO);
            demo2EsDTOS.add(demoEsDTO);
        }
        /**
         *  {@link org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository} saveAll
         *  可以发现它其实直接返回了相同类型 List<Demo2EsDTO>
         */
        List<Demo2EsDTO> result = (List<Demo2EsDTO>) demo2ElasticsearchRepository.saveAll(demo2EsDTOS);
        log.info("size:{},list:{}",result.size(),JSON.toJSONString(result));
    }

    @Test
    void delByIds(){
        List<String> ids = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            ids.add(String.valueOf(i));
        }
//        demoElasticsearchRepository.deleteAllById(ids);
        log.info("根据 _id 集合批量删除");
    }

    @Test
    void delByDes(){
        demoElasticsearchRepository.deleteByDes("测试添加Repository-saveAll");
        log.info("根据 Des 集合批量删除");
    }

    /**
     * 嵌套类查询
     *   当前是 对象 {@link UserEsDTO} name 属性 like 查询
     */
    @Test
    void getByUserEsDTO_Name(){
        List<Demo2EsDTO> list = demo2ElasticsearchRepository.getByUserEsDTO_NameLike("user");
        log.info("size:{},list:{}",list.size(),JSON.toJSONString(list));
    }

}
