package com.blacktea.redis;

import com.blacktea.redis.dto.T;
import com.blacktea.redis.service.RedisService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: black tea
 * @date: 2021/8/27 22:48
 */
@SpringBootTest
public class RedisTest {

    @Autowired
    private RedisService redisService;

    @Test
    void contextLoads() {
        testString();
    }

    void testString(){
        String key1 = "string-str-key";
        redisService.set(key1,key1);
        String key2 = "string-map-key";
        Map<String, Object> map = new HashMap<>();
        map.put("key1","string");
        map.put("key2",1);
        map.put("key3",1000L);
        map.put("key4",88.00F);
        final T obj1 = new T("testObj", 1, "testVal");
        final T obj2 = new T("testObj", 2, "testVal");
        map.put("key5",obj1);
        List<T> ts = Arrays.asList(obj1, obj2);
        map.put("key6",ts);
        redisService.set(key2,map);
    }




}
