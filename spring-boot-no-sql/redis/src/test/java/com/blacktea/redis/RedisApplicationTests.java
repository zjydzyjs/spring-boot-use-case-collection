package com.blacktea.redis;

import com.blacktea.redis.dto.T;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.*;

@SpringBootTest
class RedisApplicationTests {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void contextLoads() {
        setHasMap();
    }

    void setStr(){
        String keyStr = "testKey-myRedisConfig";
        redisTemplate.opsForValue().set(keyStr,"testVal");
        System.out.println("str:"+redisTemplate.opsForValue().get(keyStr));
    }

    /**
     * 使用默认的jdk会失败，
     * 异常如下:
     *    org.springframework.data.redis.serializer.SerializationException: Cannot serialize; nested exception is org.springframework.core.serializer.support.SerializationFailedException: Failed to serialize object using DefaultSerializer; nested exception is java.lang.IllegalArgumentException: DefaultSerializer requires a Serializable payload but received an object of type [com.blacktea.redis.RedisApplicationTests$T]
     * 如何解决:
     *    1：手动将value转为string
     *    2：配置序列化,配置一个RedisTemplate使其不进行自动配置 {@link com.blacktea.redis.config.MyRedisConfig} -> redisTemplate
     *    3: 让T实现接口 {@link java.io.Serializable}
     */
    void setObj(){
        String keyObj = "obj-my";
        redisTemplate.opsForValue().set(keyObj,new T("testObj",1,"testVal"));
        Object o = redisTemplate.opsForValue().get(keyObj);
        System.out.println("obj:"+ o);
    }

    private static Map<String,Object> map = new HashMap<>();

    static {
        map.put("key1","string");
        map.put("key2",1);
        map.put("key3",1000L);
        map.put("key4",88.00F);

    }

    void setHasMap(){
        String key1 = "hash-str-my";
        String key2 = "hash-map-my";
        final T obj1 = new T("testObj", 1, "testVal");
        final T obj2 = new T("testObj", 2, "testVal");
        map.put("key5",obj1);
        List<T> ts = Arrays.asList(obj1, obj2);
        map.put("key6",ts);
        redisTemplate.opsForValue().set(key1, map);
        redisTemplate.opsForHash().putAll(key2, map);
        System.out.println("has-str:"+redisTemplate.opsForValue().get(key1));
        System.out.println("has-map:"+redisTemplate.opsForHash().values(key2));

    }

    void setArray(){
        String arrayKey = "array-key-my";
        List<T> arrValue = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            final T obj = new T("testObj", i, "testVal");
            arrValue.add(obj);
        }
        redisTemplate.opsForList().rightPushAll(arrayKey,arrValue);
        List range = redisTemplate.opsForList().range(arrayKey, 0, -1);
        System.out.println("array:"+ range);

    }


}
