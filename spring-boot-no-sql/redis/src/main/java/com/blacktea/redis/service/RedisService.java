package com.blacktea.redis.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author: black tea
 * @date: 2021/8/27 22:07
 */
public interface RedisService {

    void set(final String key, Object value);

    boolean set(final String key, Object value, Long expireTime , TimeUnit timeUnit);

    void remove(final String... keys);

    void removePattern(final String pattern);

    void remove(final String key);

    boolean exists(final String key);

    Object get(final String key);

    void hPutMap(String k, Map<Object,Object> map);

    void hmSet(String key, Object hashKey, Object value);

    Object hmGet(String key, Object hashKey);

    void lRPush(String k,Object v);

    void lPush(String k,Object v,boolean leftStatus);

    List<Object> lRange(String k, long start, long end);

    void add(String key,Object value);

     Set<Object> setMembers(String key);

    void zAdd(String key,Object value,double scoure);

    Set<Object> rangeByScore(String key,double scoure,double scoure1);


}
