package com.blacktea.redis.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: black tea
 * @date: 2021/8/27 22:57
 */
@Data
@AllArgsConstructor
public class T implements Serializable {
    private static final long serialVersionUID = 1875172526779913430L;
    private String name;
    private Integer index;
    private String value;
}