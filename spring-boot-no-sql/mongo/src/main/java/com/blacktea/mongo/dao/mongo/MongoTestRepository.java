package com.blacktea.mongo.dao.mongo;

import com.blacktea.mongo.entites.dto.MongoTestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description:
 * @author: black tea
 * @date: 2021/8/30 22:28
 */
@Repository
public interface MongoTestRepository extends MongoRepository<MongoTestDTO,Long> {

    List<MongoTestDTO> findByDes(String des);

    List<MongoTestDTO> findByData_Name(String data_name);

    Page<MongoTestDTO> findAllByDesLike(String des, PageRequest pagable);
}
