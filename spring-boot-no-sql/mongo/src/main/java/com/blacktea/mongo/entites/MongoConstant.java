package com.blacktea.mongo.entites;

/**
 * @description:
 * @author: black tea
 * @date: 2021/8/30 14:19
 */
public class MongoConstant {

    /**
     * mongo 集合名称枚举
     */
    public enum MongoCollectEnum {
        /** 测试mongo集合名称 **/
        TEST_DEMO,
        TEST_1
        ;
    }

    /**
     * mongo Criteria where 操作
     */
    public enum CriteriaOperationEnum {
        IS,
        NE,
        LT,
        LET,
        GT,
        GTE,
        IN,
        NIN,
        MOD,
        ALL,
        SIZE,
        EXISTS,
        TYPE,
        NOT,
        REGEX,
        // 待补充
        ;
    }

}
