package com.blacktea.mongo.service;

import com.blacktea.mongo.entites.dto.MongoConditionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: black tea
 * @date: 2021/8/30 10:14
 */
public interface MongoService<T> {

    T getOneByOne(Class<T> var1,String var2, Object var3);

    T getOneByMany(Class<T> var1, Map<String,Object> var2);

    T getOneByOne(Class<T> var1,String var2, Object var3, String collectionName);

    T getOneByMany(Class<T> var1, Map<String,Object> var2, String collectionName);

    T getOneByMany(Class<T> var1, List<MongoConditionDTO> dtos, String collectionName);

    List<T> getListByOne(Class<T> var1,String var2, Object var3);

    List<T> getListByOne(Class<T> var1,String var2, Object var3, String collectionName);

    List<T> getListByMany(Class<T> var1,Map<String, Object> var2);

    List<T> getListByMany(Class<T> var1,Map<String, Object> var2, String collectionName);

    List<T> getListByCondition(Class<T> var1, List<MongoConditionDTO> dtos, String collectionName);

    List<T> getListByCondition(Class<T> var1, List<MongoConditionDTO> dtos);

    List<T> getList(Class<T> var1);

    List<T> getList(Class<T> var1, String collectionName);

    Page<T> getPage(Class<T> var1, String collectionName, PageRequest pageRequest);

    Page<T> getPage(Class<T> var1, PageRequest pageRequest);

    Page<T> getPageByCondition(Class<T> var1, String collectionName, List<MongoConditionDTO> dtos,PageRequest pageRequest);

    Page<T> getPageByCondition(Class<T> var1, List<MongoConditionDTO> dtos ,PageRequest pageRequest);

    long getCountAll(Class<T> var1);

    long getCountAll(Class<T> var1, String collectionName);

    long getCountByCondition(Class<T> var1, List<MongoConditionDTO> dtos);

    long getCountByCondition(Class<T> var1, String collectionName, List<MongoConditionDTO> dtos);

    boolean removeByCondition(Class<?> var1, List<MongoConditionDTO> dtos, String collectionName);

    boolean removeObj(Object object, String collectionName);

    T save(T objectToSave, String collectionName);

    List<T> saveAll(List<T> objectToSaves,String collectionName);

    boolean updateByCondition(Class<?> var1, List<MongoConditionDTO> dtos, Map<String,Object> updateMap , String collectionName);
}
