package com.blacktea.mongo.entites.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @description:
 * @author: black tea
 * @date: 2021/8/30 14:28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "TEST_DEMO")
public class MongoTestDTO {

    @Id
    private Long id;
    private String des;
    private Date date;
    private T data;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class T {
        private String name;
        private Integer index;
    }
}
