package com.blacktea.mongo.entites.dto;

import com.blacktea.mongo.entites.MongoConstant;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @description: mongo 条件 DTO
 *      用于在 {@link com.blacktea.mongo.service.MongoServiceImpl} 中的 XXXByCondition() 方法使用
 *
 * @author: black tea
 * @date: 2021/8/30 10:54
 */
@Data
@AllArgsConstructor
public class MongoConditionDTO implements Comparable<Integer>{

    /** 字段名称 **/
    private String key;
    /** 字段值 **/
    private Object value;
    /** 是否是并集关系 **/
    private boolean and;
    /** 顺序,升序,确保拼接顺序 **/
    private int index;
    /** 本次条件操作 **/
    private MongoConstant.CriteriaOperationEnum criteriaOperation;

    @Override
    public int compareTo(Integer o) {
        if (this.index > o){
            return 1;
        }
        return -1;
    }
}
