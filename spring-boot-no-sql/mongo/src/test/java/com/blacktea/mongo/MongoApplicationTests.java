package com.blacktea.mongo;

import cn.hutool.json.JSONUtil;
import com.blacktea.mongo.dao.mongo.MongoTestRepository;
import com.blacktea.mongo.entites.MongoConstant;
import com.blacktea.mongo.entites.dto.MongoConditionDTO;
import com.blacktea.mongo.entites.dto.MongoTestDTO;
import com.blacktea.mongo.service.MongoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.*;


@SpringBootTest
class MongoApplicationTests {

    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private MongoService<MongoTestDTO> mongoService;
    @Autowired
    private MongoTestRepository mongoTestRepository;

    @Test
    void contextLoads() {
        this.testPage();
    }

    void saveMongoService(){
        MongoTestDTO.T t = new MongoTestDTO.T("1",0);
        MongoTestDTO dto = new MongoTestDTO(1000L,"测试第一次2", new Date(), t);
        MongoTestDTO save = mongoService.save(dto, MongoConstant.MongoCollectEnum.TEST_DEMO.toString());
        System.out.println("mongoService保存后的结果:"+save);
    }

    void saveAll(){
        List<MongoTestDTO> dtos = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            MongoTestDTO.T t = new MongoTestDTO.T(String.valueOf(i),i);
            MongoTestDTO dto = new MongoTestDTO(1000L+i,"测试第一次", new Date(), t);
            dtos.add(dto);
        }
        List<MongoTestDTO> mongoTestDTOS = mongoService.saveAll(dtos, MongoConstant.MongoCollectEnum.TEST_DEMO.toString());
        System.out.println("mongoService保存后的结果:"+ JSONUtil.toJsonStr(mongoTestDTOS));
    }

    void removeObj(){
        MongoTestDTO.T t = new MongoTestDTO.T("1",0);
        MongoTestDTO dto = new MongoTestDTO(1000L,"测试第一次", new Date(), t);
        boolean result = mongoService.removeObj(dto, MongoConstant.MongoCollectEnum.TEST_DEMO.toString());
        System.out.println("按对象移除"+(result == true?"成功":"失败")+"!");
    }

    void removeAll(){
        List<MongoConditionDTO> dtos = new ArrayList<>();
        MongoConditionDTO dto1 = new MongoConditionDTO("des", "测试第一次", true, 1,MongoConstant.CriteriaOperationEnum.IS);
        MongoConditionDTO dto2 = new MongoConditionDTO("data.name", "8", true, 2,MongoConstant.CriteriaOperationEnum.IS);
        dtos.add(dto1);
        dtos.add(dto2);
        boolean result = mongoService.removeByCondition(MongoTestDTO.class,dtos, MongoConstant.MongoCollectEnum.TEST_DEMO.toString());
        System.out.println("按条件移除"+(result == true?"成功":"失败")+"!");
    }

    void update(){
        MongoConditionDTO dto1 = new MongoConditionDTO("des", "测试第一次", true, 1,MongoConstant.CriteriaOperationEnum.IS);
        MongoConditionDTO dto2 = new MongoConditionDTO("data.index", 9, false, 2,MongoConstant.CriteriaOperationEnum.IS);
        MongoConditionDTO dto3 = new MongoConditionDTO("data.index", 10, false, 2,MongoConstant.CriteriaOperationEnum.IS);
        MongoConditionDTO dto4 = new MongoConditionDTO("data.index", 9, false, 2,MongoConstant.CriteriaOperationEnum.GTE);
        List<MongoConditionDTO> dtos = Arrays.asList(
//                dto1,dto2,dto3
                dto4
        );
        // 修改值 map, 每一个 k,v 都是一组改变(set),类似mysql -> {update table_name set k1=v1,k2=v2}
        Map<String, Object> updateMap = new HashMap<>();
        updateMap.put("data.name","999");
        mongoService.updateByCondition(MongoTestDTO.class,dtos,updateMap,MongoConstant.MongoCollectEnum.TEST_DEMO.toString());
    }

    void findAll(){
        List<MongoTestDTO> list = mongoService.getList(MongoTestDTO.class,MongoConstant.MongoCollectEnum.TEST_DEMO.toString());
        System.out.println("获取到的全部数据:"+list);
    }

    void findByCondirtion(){
        MongoConditionDTO dto1 = new MongoConditionDTO("des", "测试第一次", true, 1,MongoConstant.CriteriaOperationEnum.IS);
        MongoConditionDTO dto2 = new MongoConditionDTO("data.name", "999", false, 1,MongoConstant.CriteriaOperationEnum.IS);
        List<MongoConditionDTO> dtos = Arrays.asList(dto1,dto2);
        List<MongoTestDTO> listByCondition = mongoService.getListByCondition(MongoTestDTO.class, dtos, MongoConstant.MongoCollectEnum.TEST_DEMO.toString());
        System.out.println("获取到的满足条件的数据:"+listByCondition);
    }
    void findBXX(){
        System.out.println(mongoService.getOneByOne(MongoTestDTO.class, "des", "测试第一次", MongoConstant.MongoCollectEnum.TEST_DEMO.toString()));
    }

    void findDate(){
        MongoConditionDTO dto1 = new MongoConditionDTO("date", new Date(), true, 1,MongoConstant.CriteriaOperationEnum.LET);
        List<MongoConditionDTO> dtos = Arrays.asList(dto1);
        MongoTestDTO oneByMany = mongoService.getOneByMany(MongoTestDTO.class, dtos, MongoConstant.MongoCollectEnum.TEST_DEMO.toString());
        System.out.println(oneByMany);
    }


    void testRepository(){
        System.out.println(mongoTestRepository.findByDes("测试第一次"));
        System.err.println(mongoTestRepository.findByData_Name("999"));
    }


    void testPage(){
        // limit 表示要查询条数,page表示页数, page=0 and limit =5 实际效果等于 第一页展示获取5条
        int limit = 5;
        int page = 0;
        PageRequest of = PageRequest.of(page, limit);
        Page<MongoTestDTO> findPage = mongoTestRepository.findAllByDesLike("测试第一次", of);
        System.out.println("总条数:"+findPage.getTotalElements()+",list.size=,"+findPage.getContent().size()+",list:"+JSONUtil.toJsonStr(findPage.getContent()));

        /*
            Query query = new Query();
            query.addCriteria(Criteria.where("des").regex("测试第一次"));
            long count = mongoTemplate.count(query, MongoTestDTO.class);
            query.with(of);
            List<MongoTestDTO> mongoTestDTOS = mongoTemplate.find(query, MongoTestDTO.class);

            下面的调用就是对上面注释的封装
        */

        MongoConditionDTO dto1 = new MongoConditionDTO("des", "测试第一次", true, 1,MongoConstant.CriteriaOperationEnum.REGEX);
        Page<MongoTestDTO> pageByCondition = mongoService.getPageByCondition(MongoTestDTO.class, Arrays.asList(dto1), of);
        System.err.println("总条数:"+pageByCondition.getTotalElements()+",list.size="+pageByCondition.getContent().size()+",list:"+JSONUtil.toJsonStr(pageByCondition.getContent()));
    }

}
