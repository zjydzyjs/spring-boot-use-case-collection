package com.blacktea.springsecuritydemo.service;

import com.blacktea.springsecuritydemo.entites.domain.UmsResource;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台资源表 服务类
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
public interface UmsResourceService extends IService<UmsResource> {

}
