package com.blacktea.springsecuritydemo.common;

/**
 * @description: 封装API的错误码
 * @author: black tea
 * @date: 2021/10/7 14:15
 */
public interface IErrorCode {
    long getCode();

    String getMessage();
}