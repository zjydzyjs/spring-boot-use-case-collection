package com.blacktea.springsecuritydemo.entites.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.blacktea.springsecuritydemo.entites.BasicObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 后台用户表
 * </p>
 *
 * @author blacktea
 * @since 2021-10-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_admin")
public class UmsAdmin extends BasicObject<UmsAdmin> {

    private static final long serialVersionUID = 1L;

    private String username;

    private String password;

    /**
     * 头像
     */
    private String icon;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 备注信息
     */
    private String note;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后登录时间
     */
    private Date loginTime;

    /**
     * 帐号启用状态：0->禁用；1->启用
     */
    private Integer status;

}
