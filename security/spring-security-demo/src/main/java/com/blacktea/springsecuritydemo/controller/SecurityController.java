package com.blacktea.springsecuritydemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: black tea
 * @date: 2021/10/7 13:35
 */
@RequestMapping("test/security/")
@RestController
public class SecurityController {

    @GetMapping("get1/{param}")
    public Object get11(@PathVariable String param){
        return param;
    }
}
