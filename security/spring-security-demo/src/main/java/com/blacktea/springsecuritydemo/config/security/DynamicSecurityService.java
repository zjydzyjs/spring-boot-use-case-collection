package com.blacktea.springsecuritydemo.config.security;

import org.springframework.security.access.ConfigAttribute;

import java.util.Map;

/**
 * @description: 动态权限相关业务类
 * @author: black tea
 * @date: 2021/10/8 9:34
 */
@FunctionalInterface
public interface DynamicSecurityService {

    /**
     * 加载资源ANT通配符和资源对应MAP
     */
    Map<String, ConfigAttribute> loadDataSource();
}
