package com.blacktea.springsecuritydemo.service.impl;

import com.blacktea.springsecuritydemo.entites.domain.UmsResource;
import com.blacktea.springsecuritydemo.dao.mapper.UmsResourceMapper;
import com.blacktea.springsecuritydemo.service.UmsResourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台资源表 服务实现类
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
@Service
public class UmsResourceServiceImpl extends ServiceImpl<UmsResourceMapper, UmsResource> implements UmsResourceService {

}
