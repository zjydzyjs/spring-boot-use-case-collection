package com.blacktea.springsecuritydemo.entites;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: black tea
 * @date: 2021/7/28 10:37
 */
@Data
public class BasicObject<T extends Model<?>> extends Model<T> {

    //这个用的也是雪花算法
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
