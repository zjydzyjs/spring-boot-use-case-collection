package com.blacktea.springsecuritydemo.dao.mapper;

import com.blacktea.springsecuritydemo.entites.domain.UmsAdminResourceRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台用户和权限关系表(除角色中定义的权限以外的加减权限) Mapper 接口
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
public interface UmsAdminResourceRelationMapper extends BaseMapper<UmsAdminResourceRelation> {

}
