package com.blacktea.springsecuritydemo.dao.mapper;

import com.blacktea.springsecuritydemo.entites.domain.UmsResource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台资源表 Mapper 接口
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
public interface UmsResourceMapper extends BaseMapper<UmsResource> {

}
