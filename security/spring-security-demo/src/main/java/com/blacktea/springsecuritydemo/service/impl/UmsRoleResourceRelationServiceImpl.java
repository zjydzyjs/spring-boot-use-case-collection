package com.blacktea.springsecuritydemo.service.impl;

import com.blacktea.springsecuritydemo.entites.domain.UmsRoleResourceRelation;
import com.blacktea.springsecuritydemo.dao.mapper.UmsRoleResourceRelationMapper;
import com.blacktea.springsecuritydemo.service.UmsRoleResourceRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台角色资源关系表 服务实现类
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
@Service
public class UmsRoleResourceRelationServiceImpl extends ServiceImpl<UmsRoleResourceRelationMapper, UmsRoleResourceRelation> implements UmsRoleResourceRelationService {

}
