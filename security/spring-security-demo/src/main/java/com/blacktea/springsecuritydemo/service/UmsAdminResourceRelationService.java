package com.blacktea.springsecuritydemo.service;

import com.blacktea.springsecuritydemo.entites.domain.UmsAdminResourceRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台用户和权限关系表(除角色中定义的权限以外的加减权限) 服务类
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
public interface UmsAdminResourceRelationService extends IService<UmsAdminResourceRelation> {

}
