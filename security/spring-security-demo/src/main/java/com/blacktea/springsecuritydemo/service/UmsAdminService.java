package com.blacktea.springsecuritydemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blacktea.springsecuritydemo.entites.domain.UmsAdmin;
import com.blacktea.springsecuritydemo.entites.domain.UmsResource;

import java.util.List;

/**
 * <p>
 * 后台用户表 服务类
 * </p>
 *
 * @author blacktea
 * @since 2021-10-07
 */
public interface UmsAdminService extends IService<UmsAdmin> {

    /**
     * 根据用户名获取后台管理员
     */
    UmsAdmin getAdminByUsername(String username);

    /**
     * 注册功能
     */
    UmsAdmin register(UmsAdmin umsAdminParam);

    /**
     * 登录功能
     * @param username 用户名
     * @param password 密码
     * @return 生成的JWT的token
     */
    String login(String username, String password);

    /**
     * 获取用户所有资源（包括角色资源和+-资源）
     */
    List<UmsResource> getResourceList(Long adminId);
}
