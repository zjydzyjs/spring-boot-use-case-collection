package com.blacktea.springsecuritydemo.entites.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.blacktea.springsecuritydemo.entites.BasicObject;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 后台用户和权限关系表(除角色中定义的权限以外的加减权限)
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_admin_resource_relation")
public class UmsAdminResourceRelation extends BasicObject<UmsAdminResourceRelation> {

    private static final long serialVersionUID = 1L;

    private Long adminId;

    private Long resourceId;

    private Integer type;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
