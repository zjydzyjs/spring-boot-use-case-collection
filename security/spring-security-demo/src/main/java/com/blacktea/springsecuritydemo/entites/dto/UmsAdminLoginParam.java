package com.blacktea.springsecuritydemo.entites.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;


/**
 * @description:
 * @author: black tea
 * @date: 2021/10/7 16:19
 */
@Data
public class UmsAdminLoginParam {

//    @ApiModelProperty(value = "用户名", required = true)
    @NotBlank(message = "用户名不能为空")
    private String username;
//    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = "密码不能为空")
    private String password;
}
