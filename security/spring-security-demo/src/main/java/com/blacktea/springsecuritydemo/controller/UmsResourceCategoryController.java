package com.blacktea.springsecuritydemo.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 资源分类表 前端控制器
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
@Controller
@RequestMapping("/umsResourceCategory")
public class UmsResourceCategoryController {

}

