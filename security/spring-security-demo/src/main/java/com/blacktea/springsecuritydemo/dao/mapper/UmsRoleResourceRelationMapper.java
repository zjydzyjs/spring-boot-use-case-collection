package com.blacktea.springsecuritydemo.dao.mapper;

import com.blacktea.springsecuritydemo.entites.domain.UmsRoleResourceRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台角色资源关系表 Mapper 接口
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
public interface UmsRoleResourceRelationMapper extends BaseMapper<UmsRoleResourceRelation> {

}
