package com.blacktea.springsecuritydemo.service.impl;

import com.blacktea.springsecuritydemo.entites.domain.UmsResourceCategory;
import com.blacktea.springsecuritydemo.dao.mapper.UmsResourceCategoryMapper;
import com.blacktea.springsecuritydemo.service.UmsResourceCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资源分类表 服务实现类
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
@Service
public class UmsResourceCategoryServiceImpl extends ServiceImpl<UmsResourceCategoryMapper, UmsResourceCategory> implements UmsResourceCategoryService {

}
