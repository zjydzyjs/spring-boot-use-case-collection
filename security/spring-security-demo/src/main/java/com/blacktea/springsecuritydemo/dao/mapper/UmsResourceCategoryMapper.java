package com.blacktea.springsecuritydemo.dao.mapper;

import com.blacktea.springsecuritydemo.entites.domain.UmsResourceCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 资源分类表 Mapper 接口
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
public interface UmsResourceCategoryMapper extends BaseMapper<UmsResourceCategory> {

}
