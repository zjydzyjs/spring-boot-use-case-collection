package com.blacktea.springsecuritydemo.controller;


import com.blacktea.springsecuritydemo.common.CommonResult;
import com.blacktea.springsecuritydemo.config.security.DynamicSecurityMetadataSource;
import com.blacktea.springsecuritydemo.entites.domain.UmsResource;
import com.blacktea.springsecuritydemo.service.UmsResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 后台资源表 前端控制器
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */

@RestController
//@Api(tags = "UmsResourceController", description = "后台资源管理")
@RequestMapping("/ums/resource")
public class UmsResourceController {

    @Autowired
    private UmsResourceService resourceService;
    @Autowired
    private DynamicSecurityMetadataSource dynamicSecurityMetadataSource;

//    @ApiOperation("添加后台资源")
    @PostMapping(value = "/create")
    public CommonResult create(@RequestBody UmsResource umsResource) {
        if (resourceService.save(umsResource)) {
            dynamicSecurityMetadataSource.clearDataSource();
            return CommonResult.success(true);
        } else {
            return CommonResult.failed();
        }
    }
}


