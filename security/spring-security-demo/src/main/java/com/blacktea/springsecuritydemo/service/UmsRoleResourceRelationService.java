package com.blacktea.springsecuritydemo.service;

import com.blacktea.springsecuritydemo.entites.domain.UmsRoleResourceRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台角色资源关系表 服务类
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
public interface UmsRoleResourceRelationService extends IService<UmsRoleResourceRelation> {

}
