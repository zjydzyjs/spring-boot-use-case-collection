package com.blacktea.springsecuritydemo.service;

import com.blacktea.springsecuritydemo.entites.domain.UmsResourceCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 资源分类表 服务类
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
public interface UmsResourceCategoryService extends IService<UmsResourceCategory> {

}
