package com.blacktea.springsecuritydemo.entites.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.blacktea.springsecuritydemo.entites.BasicObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 资源分类表
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_resource_category")
public class UmsResourceCategory extends BasicObject<UmsResourceCategory> {

    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 排序
     */
    private Integer sort;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
