package com.blacktea.springsecuritydemo.dao.mapper;

import com.blacktea.springsecuritydemo.entites.domain.UmsAdmin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台用户表 Mapper 接口
 * </p>
 *
 * @author blacktea
 * @since 2021-10-07
 */
public interface UmsAdminMapper extends BaseMapper<UmsAdmin> {

}
