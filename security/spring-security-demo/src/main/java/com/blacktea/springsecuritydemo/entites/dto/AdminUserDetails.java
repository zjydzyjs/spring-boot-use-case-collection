package com.blacktea.springsecuritydemo.entites.dto;

import com.blacktea.springsecuritydemo.entites.domain.UmsAdmin;
import com.blacktea.springsecuritydemo.entites.domain.UmsResource;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description: SpringSecurity需要的用户详情
 * @author: black tea
 * @date: 2021/10/7 14:44
 */
@RequiredArgsConstructor
public class AdminUserDetails implements UserDetails {

    private final UmsAdmin umsAdmin;
    private final List<UmsResource> resourceList;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //返回当前用户的权限
        return resourceList.stream()
                .filter(resource -> resource.getName() != null)
                .map(resource -> new SimpleGrantedAuthority(resource.getId()+":"+resource.getName()))
                .collect(Collectors.toSet());
    }

    @Override
    public String getPassword() {
        return umsAdmin.getPassword();
    }

    @Override
    public String getUsername() {
        return umsAdmin.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return umsAdmin.getStatus().equals(1);
    }
}
