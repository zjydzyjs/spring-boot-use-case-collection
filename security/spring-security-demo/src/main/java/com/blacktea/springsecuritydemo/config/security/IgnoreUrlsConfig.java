package com.blacktea.springsecuritydemo.config.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashSet;
import java.util.Set;

/**
 * @description: 用于配置不需要保护的资源路径
 * @author: black tea
 * @date: 2021/10/8 9:14
 */
@Getter
@Setter
@ConfigurationProperties(
        prefix = "security.ignored"
)
public class IgnoreUrlsConfig {

    private Set<String> urls = new HashSet<>();

}
