package com.blacktea.springsecuritydemo.entites.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.blacktea.springsecuritydemo.entites.BasicObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 后台角色资源关系表
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_role_resource_relation")
public class UmsRoleResourceRelation extends BasicObject<UmsRoleResourceRelation> {

    private static final long serialVersionUID = 1L;

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 资源ID
     */
    private Long resourceId;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
