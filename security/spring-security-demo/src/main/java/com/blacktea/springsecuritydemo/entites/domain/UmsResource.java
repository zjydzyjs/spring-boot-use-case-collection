package com.blacktea.springsecuritydemo.entites.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.blacktea.springsecuritydemo.entites.BasicObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * <p>
 * 后台资源表
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ums_resource")
public class UmsResource extends BasicObject<UmsResource> {

    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 资源名称
     */
    private String name;

    /**
     * 资源URL
     */
    private String url;

    /**
     * 描述
     */
    private String description;

    /**
     * 资源分类ID
     */
    private Long categoryId;

}
