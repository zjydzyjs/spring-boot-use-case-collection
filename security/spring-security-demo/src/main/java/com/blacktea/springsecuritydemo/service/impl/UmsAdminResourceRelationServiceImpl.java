package com.blacktea.springsecuritydemo.service.impl;

import com.blacktea.springsecuritydemo.entites.domain.UmsAdminResourceRelation;
import com.blacktea.springsecuritydemo.dao.mapper.UmsAdminResourceRelationMapper;
import com.blacktea.springsecuritydemo.service.UmsAdminResourceRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台用户和权限关系表(除角色中定义的权限以外的加减权限) 服务实现类
 * </p>
 *
 * @author blacktea
 * @since 2021-10-08
 */
@Service
public class UmsAdminResourceRelationServiceImpl extends ServiceImpl<UmsAdminResourceRelationMapper, UmsAdminResourceRelation> implements UmsAdminResourceRelationService {

}
