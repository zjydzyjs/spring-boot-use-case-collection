package com.blacktea.magicapi.base;

/**
 * @description:
 * @author: black tea
 * @date: 2021/12/31 9:46
 */
public interface ResultCode {

    Integer getCode();

    String getMessage();
}
