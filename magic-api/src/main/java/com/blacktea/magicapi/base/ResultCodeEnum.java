package com.blacktea.magicapi.base;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @description:
 * @author: black tea
 * @date: 2021/12/31 9:47
 */
@Getter
@AllArgsConstructor
public enum ResultCodeEnum implements ResultCode{
    /** 无法判断处理的异常 **/
    UNKNOWN_EXCEPTION(500,"系统内部出现错误!"),

    TEST_ERROR(10000,"测试失败!")
    ;

    private Integer code;

    private String message;

}
