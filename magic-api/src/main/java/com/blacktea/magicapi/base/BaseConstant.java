package com.blacktea.magicapi.base;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @description:
 * @author: black tea
 * @date: 2021/12/31 15:01
 */
public class BaseConstant {

    /**
     * 数据源常量
     */
    public static final String RESULT_SUCCESS_MESSAGE = "success";
    public static final String DATASOURCE_MASTER = "master";

    /**
     * magic-api ui用户权限枚举
     */
    @AllArgsConstructor
    @Getter
    public enum MagicUiPermissionTypeEnum {
        // magic-api 相关ui权限
        PAGE_BUTTON_PERMISSION(1,"是否拥有页面按钮的权限,是否允许访问"),
        INTERFACE_PERMISSION(2,"是否拥有对该接口的增删改权限"),
        FUNCTION_PERMISSION(3,"是否拥有对该函数的增删改权限"),
        GROUPING_PERMISSION(4,"是否拥有对该分组的增删改权限"),
        DATASOURCE_PERMISSION(5,"是否拥有对该数据源的增删改权限");

        private Integer code;
        private String title;
    }

    /**
     * magic-api ui 用户登录所用常量
     */
    public static final String BLACKLIST_IPS_KEY = "login_blacklist_ips";
    public static final String WHITELIST_IPS_KEY = "login_blacklist_ips";

    public static final class Symbol{
        public static final String COMMA = ",";
        public static final String COLON = ":";
        public static final String DOUBLE_COLON = COLON+COLON;
        public static final String ASTERISK = "*";
    }
}
