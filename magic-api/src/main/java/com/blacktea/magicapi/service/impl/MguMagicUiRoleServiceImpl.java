package com.blacktea.magicapi.service.impl;

import com.blacktea.magicapi.entites.domain.MguMagicUiRole;
import com.blacktea.magicapi.dao.mapper.MguMagicUiRoleMapper;
import com.blacktea.magicapi.service.MguMagicUiRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * magic_ui角色表 服务实现类
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
@Service
public class MguMagicUiRoleServiceImpl extends ServiceImpl<MguMagicUiRoleMapper, MguMagicUiRole> implements MguMagicUiRoleService {

}
