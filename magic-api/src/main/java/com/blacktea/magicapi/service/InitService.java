package com.blacktea.magicapi.service;

/**
 * @description:
 * @author: black tea
 * @date: 2022/1/1 16:55
 */
public interface InitService {

    void initMagicApiUiUser();
}
