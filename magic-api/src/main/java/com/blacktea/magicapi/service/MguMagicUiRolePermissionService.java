package com.blacktea.magicapi.service;

import com.blacktea.magicapi.entites.domain.MguMagicUiRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * magic_ui角色权限表 服务类
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
public interface MguMagicUiRolePermissionService extends IService<MguMagicUiRolePermission> {

}
