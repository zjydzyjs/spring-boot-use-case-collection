package com.blacktea.magicapi.service;

import com.blacktea.magicapi.entites.domain.MguMagicUiUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * magic_ui用户表 服务类
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
public interface MguMagicUiUserService extends IService<MguMagicUiUser> {

}
