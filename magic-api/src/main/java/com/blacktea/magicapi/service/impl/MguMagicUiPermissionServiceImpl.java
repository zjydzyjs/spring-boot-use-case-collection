package com.blacktea.magicapi.service.impl;

import com.blacktea.magicapi.entites.domain.MguMagicUiPermission;
import com.blacktea.magicapi.dao.mapper.MguMagicUiPermissionMapper;
import com.blacktea.magicapi.service.MguMagicUiPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * magic_ui权限表 服务实现类
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
@Service
public class MguMagicUiPermissionServiceImpl extends ServiceImpl<MguMagicUiPermissionMapper, MguMagicUiPermission> implements MguMagicUiPermissionService {

}
