package com.blacktea.magicapi.service.impl;

import com.blacktea.magicapi.entites.domain.MguMagicUiUser;
import com.blacktea.magicapi.dao.mapper.MguMagicUiUserMapper;
import com.blacktea.magicapi.service.MguMagicUiUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * magic_ui用户表 服务实现类
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
@Service
public class MguMagicUiUserServiceImpl extends ServiceImpl<MguMagicUiUserMapper, MguMagicUiUser> implements MguMagicUiUserService {

}
