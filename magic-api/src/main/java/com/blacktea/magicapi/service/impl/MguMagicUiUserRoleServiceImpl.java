package com.blacktea.magicapi.service.impl;

import com.blacktea.magicapi.entites.domain.MguMagicUiUserRole;
import com.blacktea.magicapi.dao.mapper.MguMagicUiUserRoleMapper;
import com.blacktea.magicapi.service.MguMagicUiUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * magic_ui用户角色表 服务实现类
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
@Service
public class MguMagicUiUserRoleServiceImpl extends ServiceImpl<MguMagicUiUserRoleMapper, MguMagicUiUserRole> implements MguMagicUiUserRoleService {

}
