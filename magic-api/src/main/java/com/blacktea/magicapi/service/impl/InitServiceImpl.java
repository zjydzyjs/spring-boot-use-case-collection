package com.blacktea.magicapi.service.impl;

import com.blacktea.magicapi.entites.domain.MguMagicUiPermission;
import com.blacktea.magicapi.entites.domain.MguMagicUiRolePermission;
import com.blacktea.magicapi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ssssssss.magicapi.interceptor.Authorization;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: black tea
 * @date: 2022/1/1 16:56
 */
@Service
public class InitServiceImpl implements InitService {

    @Autowired
    private MguMagicUiUserService userService;
    @Autowired
    private MguMagicUiRoleService roleService;
    @Autowired
    private MguMagicUiPermissionService permissionService;
    @Autowired
    private MguMagicUiUserRoleService userRoleService;
    @Autowired
    private MguMagicUiRolePermissionService rolePermissionService;


    @Override
//    @PostConstruct
    public void initMagicApiUiUser() {
//        MguMagicUiUser magicUiUser = new MguMagicUiUser();
//        magicUiUser.setUsername("blacktea");
//        magicUiUser.setPassword("blacktea");
//        userService.save(magicUiUser);
//
//        MguMagicUiRole role1 = new MguMagicUiRole();
//        role1.setRoleName("超级管理员");
//        MguMagicUiRole role2 = new MguMagicUiRole();
//        role2.setRoleName("管理员");
//        roleService.saveBatch(Arrays.asList(role1,role2));
//
//        // 页面按钮
//        Set<MguMagicUiPermission> pageButtonPermissions = Arrays.stream(Authorization.values()).map(a -> {
//            Long pageButtonType = BaseConstant.MagicUiPermissionTypeEnum.PAGE_BUTTON_PERMISSION.getCode();
//            MguMagicUiPermission permission = new MguMagicUiPermission();
//            permission.setType(pageButtonType);
//            String name = "";
//            switch (a) {
//                case SAVE:
//                    name = "保存";
//                    break;
//                case DELETE:
//                    name = "删除";
//                    break;
//                case VIEW:
//                    name = "查询";
//                    break;
//                case DOWNLOAD:
//                    name = "导出";
//                case UPLOAD:
//                    name = "上传";
//                    break;
//                case PUSH:
//                    name = "推送";
//                    break;
//                default:
//                    break;
//            }
//            permission.setName(name);
//            permission.setAuthorization(a.name());
//            return permission;
//        }).collect(Collectors.toSet());
//        permissionService.saveBatch(pageButtonPermissions);

    }

//    @PostConstruct
    public void initPermissions(){
        // 生成所有权限，(目前有,1:页面按钮,2:接口的增删改,3:函数的增删改,4:分组的增删改,5:数据源的增删改)
        List<MguMagicUiPermission> permissions = new ArrayList<>();
        // 1. 已完成
        // 2. 接口的增删改
        Integer type = 2;
        MguMagicUiPermission save1 = new MguMagicUiPermission();
        save1.setType(type);
        save1.setAuthorization(Authorization.SAVE.name());
        save1.setName("保存接口");
        permissions.add(save1);
        MguMagicUiPermission get1 = new MguMagicUiPermission();
        get1.setType(type);
        get1.setAuthorization(Authorization.VIEW.name());
        get1.setName("查询接口");
        permissions.add(get1);
        MguMagicUiPermission delete1 = new MguMagicUiPermission();
        delete1.setType(type);
        delete1.setAuthorization(Authorization.DELETE.name());
        delete1.setName("删除接口");
        permissions.add(delete1);
        // 3. 函数的增删改
        type = 3;
        MguMagicUiPermission save2 = new MguMagicUiPermission();
        save2.setType(type);
        save2.setAuthorization(Authorization.SAVE.name());
        save2.setName("保存函数");
        permissions.add(save2);
        MguMagicUiPermission get2 = new MguMagicUiPermission();
        get2.setType(type);
        get2.setAuthorization(Authorization.VIEW.name());
        get2.setName("查询函数");
        permissions.add(get2);
        MguMagicUiPermission delete2 = new MguMagicUiPermission();
        delete2.setType(type);
        delete2.setAuthorization(Authorization.DELETE.name());
        delete2.setName("删除函数");
        permissions.add(delete2);
        // 4. 分组的增删改
        type = 4;
        MguMagicUiPermission save3 = new MguMagicUiPermission();
        save3.setType(type);
        save3.setAuthorization(Authorization.SAVE.name());
        save3.setName("保存分组");
        permissions.add(save3);
        MguMagicUiPermission get3 = new MguMagicUiPermission();
        get3.setType(type);
        get3.setAuthorization(Authorization.VIEW.name());
        get3.setName("查询分组");
        permissions.add(get3);
        MguMagicUiPermission delete3 = new MguMagicUiPermission();
        delete3.setType(type);
        delete3.setAuthorization(Authorization.DELETE.name());
        delete3.setName("删除分组");
        permissions.add(delete3);
        // 5. 数据源的增删改
        type = 5;
        MguMagicUiPermission save4 = new MguMagicUiPermission();
        save4.setType(type);
        save4.setAuthorization(Authorization.SAVE.name());
        save4.setName("保存数据源");
        permissions.add(save4);
        MguMagicUiPermission get4 = new MguMagicUiPermission();
        get4.setType(type);
        get4.setAuthorization(Authorization.VIEW.name());
        get4.setName("查询数据源");
        permissions.add(get4);
        MguMagicUiPermission delete4 = new MguMagicUiPermission();
        delete4.setType(type);
        delete4.setAuthorization(Authorization.DELETE.name());
        delete4.setName("删除数据源");
        permissions.add(delete4);
        permissionService.saveBatch(permissions);
    }

//    @PostConstruct
    void initRolePermission(){
        // 将所有的权限与超级管理员进行绑定
        String adminRoleId = "1477219222060888065";
        // 获取所有权限
        List<MguMagicUiPermission> mguMagicUiPermissions = permissionService.list();
        List<MguMagicUiRolePermission> mguMagicUiRolePermissions = mguMagicUiPermissions.stream().map(p -> {
            MguMagicUiRolePermission mguMagicUiRolePermission = new MguMagicUiRolePermission();
            mguMagicUiRolePermission.setRoleId(adminRoleId);
            mguMagicUiRolePermission.setPermissionsId(p.getId());
            return mguMagicUiRolePermission;
        }).collect(Collectors.toList());
        rolePermissionService.saveBatch(mguMagicUiRolePermissions);
    }
}
