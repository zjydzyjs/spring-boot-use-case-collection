package com.blacktea.magicapi.service.impl;

import com.blacktea.magicapi.entites.domain.MguMagicUiRolePermission;
import com.blacktea.magicapi.dao.mapper.MguMagicUiRolePermissionMapper;
import com.blacktea.magicapi.service.MguMagicUiRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * magic_ui角色权限表 服务实现类
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
@Service
public class MguMagicUiRolePermissionServiceImpl extends ServiceImpl<MguMagicUiRolePermissionMapper, MguMagicUiRolePermission> implements MguMagicUiRolePermissionService {

}
