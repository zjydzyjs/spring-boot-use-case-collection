package com.blacktea.magicapi.service;

import com.blacktea.magicapi.entites.domain.MguMagicUiUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * magic_ui用户角色表 服务类
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
public interface MguMagicUiUserRoleService extends IService<MguMagicUiUserRole> {

}
