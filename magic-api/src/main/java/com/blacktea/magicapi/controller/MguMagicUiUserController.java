package com.blacktea.magicapi.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * magic_ui用户表 前端控制器
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
@RestController
@RequestMapping("/mguMagicUiUser")
public class MguMagicUiUserController {

}

