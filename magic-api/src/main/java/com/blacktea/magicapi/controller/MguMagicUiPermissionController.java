package com.blacktea.magicapi.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * magic_ui权限表 前端控制器
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
@Controller
@RequestMapping("/mguMagicUiPermission")
public class MguMagicUiPermissionController {

}

