package com.blacktea.magicapi.dao.mapper;

import com.blacktea.magicapi.entites.domain.MguMagicUiPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * magic_ui权限表 Mapper 接口
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
public interface MguMagicUiPermissionMapper extends BaseMapper<MguMagicUiPermission> {

}
