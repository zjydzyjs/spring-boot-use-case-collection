package com.blacktea.magicapi.dao.mapper;

import com.blacktea.magicapi.entites.domain.MguMagicUiUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * magic_ui用户角色表 Mapper 接口
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
public interface MguMagicUiUserRoleMapper extends BaseMapper<MguMagicUiUserRole> {

}
