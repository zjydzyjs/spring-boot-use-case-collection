package com.blacktea.magicapi.entites.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.blacktea.magicapi.entites.BasicObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * magic_ui用户角色表
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("mgu_magic_ui_user_role")
public class MguMagicUiUserRole extends BasicObject<MguMagicUiUserRole> {

    private static final long serialVersionUID = 1L;

    /**
     * webmagic-ui用户id
     */
    private String userId;

    /**
     * webmagic-ui角色id
     */
    private String roleId;

    @Override
    protected Serializable pkVal() {
        return super.pkVal();
    }

}
