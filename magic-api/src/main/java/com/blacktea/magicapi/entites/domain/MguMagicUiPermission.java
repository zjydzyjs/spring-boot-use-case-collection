package com.blacktea.magicapi.entites.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.blacktea.magicapi.entites.BasicObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * magic_ui权限表
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("mgu_magic_ui_permission")
public class MguMagicUiPermission extends BasicObject<MguMagicUiPermission> {

    private static final long serialVersionUID = 1L;

    /**
     * webmagic-ui权限名称
     */
    private String name;

    /**
     * webmagic-ui权限值,具体查看类(CustomAuthorizationInterceptor与Authorization)
     */
    private String authorization;

    /**
     * webmagic-ui权限类型(目前有,1:页面按钮,2:接口的增删改,3:函数的增删改,4:分组的增删改,5:数据源的增删改)
     */
    private Integer type;

    @Override
    protected Serializable pkVal() {
        return super.pkVal();
    }

}
