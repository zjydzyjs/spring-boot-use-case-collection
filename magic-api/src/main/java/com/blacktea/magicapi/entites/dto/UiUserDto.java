package com.blacktea.magicapi.entites.dto;

import com.blacktea.magicapi.entites.domain.MguMagicUiPermission;
import com.blacktea.magicapi.entites.domain.MguMagicUiRole;
import com.blacktea.magicapi.entites.domain.MguMagicUiUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: black tea
 * @date: 2022/1/1 23:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UiUserDto {

    /** magic-api UI 用户 **/
    private MguMagicUiUser user;
    /** magic-api UI 用户角色 **/
    private List<MguMagicUiRole> role;
    /** magic-api UI 角色对应权限集合 **/
    private List<MguMagicUiPermission> permissions;
}
