package com.blacktea.magicapi.entites.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.blacktea.magicapi.entites.BasicObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * magic_ui角色权限表
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("mgu_magic_ui_role_permission")
public class MguMagicUiRolePermission extends BasicObject<MguMagicUiRolePermission> {

    private static final long serialVersionUID = 1L;

    /**
     * webmagic-ui角色id
     */
    private String roleId;

    /**
     * webmagic-ui权限id
     */
    private String permissionsId;

    @Override
    protected Serializable pkVal() {
        return super.pkVal();
    }

}
