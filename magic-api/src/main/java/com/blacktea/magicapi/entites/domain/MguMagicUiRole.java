package com.blacktea.magicapi.entites.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.blacktea.magicapi.entites.BasicObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * magic_ui角色表
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
@ToString(callSuper = true)
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("mgu_magic_ui_role")
public class MguMagicUiRole extends BasicObject<MguMagicUiRole> {

    private static final long serialVersionUID = 1L;

    /**
     * webmagic-ui角色名称
     */
    private String roleName;

    /**
     * 角色描述
     */
    private String roleDepict;

    @Override
    protected Serializable pkVal() {
        return super.pkVal();
    }

}
