package com.blacktea.magicapi.entites;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description:
 * @author: black tea
 * @date: 2021/7/28 10:37
 */
@Data
public class BasicObject<T extends Model<?>> extends Model<T> {

    @TableId(value = "id", type = IdType.NONE)
    public String id;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    public Date createDateTime;

    /**
     * 更新时间,初次时间与创建时间相同
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    public Date updateDateTime;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    public String createBy;

    /**
     * 更新者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    public String updateBy;

    /**
     * 乐观锁,默认从0开始
     */
    @Version
    @TableField(fill = FieldFill.INSERT_UPDATE)
    public Integer version;

    /**
     * 删除标识,0:未删除,1:已删除
     */
    @TableLogic
    public Long deleteFlag;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
