package com.blacktea.magicapi.entites.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.blacktea.magicapi.entites.BasicObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * magic_ui用户表
 * </p>
 *
 * @author blacktea
 * @since 2022-01-01
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@TableName("mgu_magic_ui_user")
public class MguMagicUiUser extends BasicObject<MguMagicUiUser> {

    private static final long serialVersionUID = 1L;

    /**
     * webmagic-ui用户名称
     */
    private String username;

    /**
     * webmagic-ui用户密码
     */
    private String password;

    /** 锁定当前账号 **/
    @TableField("`lock`")
    private boolean lock;

}
