package com.blacktea.magicapi.config;

import com.blacktea.magicapi.Interceptor.CustomAuthorizationInterceptor;
import com.blacktea.magicapi.policy.magicapiuiuser.UiUserRedisPolicy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.ssssssss.magicapi.interceptor.AuthorizationInterceptor;

/**
 * @description:
 * @author: black tea
 * @date: 2022/2/15 22:56
 */

/** 配置hutool的 {@link cn.hutool.extra.spring.SpringUtil} **/
@ComponentScan({"cn.hutool.extra.spring"})
@Configuration
public class MyConfig {

    @Bean
    @Primary
    public AuthorizationInterceptor customAuthorizationInterceptor1(){
        return new CustomAuthorizationInterceptor(1,new UiUserRedisPolicy());
    }
}
