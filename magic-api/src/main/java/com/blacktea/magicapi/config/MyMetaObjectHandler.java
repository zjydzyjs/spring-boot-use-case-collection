package com.blacktea.magicapi.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @description: MybatisPlus元数据自动填充handler
 * @author: black tea
 * @date: 2021/10/9 17:00
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill ....");
        this.strictInsertFill(metaObject, "createDateTime", Date::new, Date.class);
        this.strictInsertFill(metaObject, "updateDateTime", Date::new, Date.class);
        String username = "匿名用户添加";
        this.strictInsertFill(metaObject, "version", () -> 0, Integer.class);
        this.strictInsertFill(metaObject, "deleted", () -> 0, Integer.class);
        String finalUsername = username;
        this.strictInsertFill(metaObject, "createBy", () -> finalUsername, String.class);
        this.strictInsertFill(metaObject, "updateBy", () -> finalUsername, String.class);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
        this.strictUpdateFill(metaObject, "createDateTime", Date::new, Date.class);
        String username = "匿名用户更新";
        String finalUsername = username;
        this.strictInsertFill(metaObject, "updateBy", () -> finalUsername, String.class);
    }
}