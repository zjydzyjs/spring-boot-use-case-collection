package com.blacktea.magicapi.config;

import cn.hutool.core.map.MapUtil;
import com.baomidou.dynamic.datasource.provider.DynamicDataSourceProvider;
import com.blacktea.magicapi.Interceptor.CustomAuthorizationInterceptor;
import com.blacktea.magicapi.base.BaseConstant;
import com.blacktea.magicapi.provider.CustomJsonValueProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.ssssssss.magicapi.config.MagicDynamicDataSource;
import org.ssssssss.magicapi.exception.MagicAPIException;
import org.ssssssss.magicapi.interceptor.AuthorizationInterceptor;

import javax.sql.DataSource;
import java.util.Map;

/**
 * @description:
 * @author: black tea
 * @date: 2021/12/30 16:35
 */
@Configuration
public class MagicAPIConfiguration {

/*
    boot默认数据源对象 配置 配置多数据源

    @Bean(name = "primaryDataSource")
    @Qualifier("primaryDataSource")
    @ConfigurationProperties(prefix="spring.datasource.primary")
    public DataSource primaryDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "secondaryDataSource")
    @Qualifier("secondaryDataSource")
    @Primary
    @ConfigurationProperties(prefix="spring.datasource.secondary")
    public DataSource secondaryDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public MagicDynamicDataSource magicDynamicDataSource(@Qualifier("primaryDataSource") DataSource master,
                                                         @Qualifier("secondaryDataSource") DataSource slavel1){
        //设置默认数据源
        MagicDynamicDataSource dynamicDataSource = new MagicDynamicDataSource();
        dynamicDataSource.setDefault(master);
        dynamicDataSource.add("slave", slavel1);
        return dynamicDataSource;
    }
*/

    /**
     * 配置多数据源 （mybatis-plus 扩展的多数据源框架）
     *
     * @see MagicDynamicDataSource
     */
    @Bean
    public MagicDynamicDataSource magicDynamicDataSource(DynamicDataSourceProvider dynamicDataSourceProvider) {
        MagicDynamicDataSource dynamicDataSource = new MagicDynamicDataSource();

        Map<String, DataSource> dataSourceMap = dynamicDataSourceProvider.loadDataSources();
        if (MapUtil.isEmpty(dataSourceMap)){
            throw new MagicAPIException("请配置数据源");
        }
        dataSourceMap.forEach((k,v) -> {
            if (BaseConstant.DATASOURCE_MASTER.equals(k)) {
                // 设置默认数据源
                dynamicDataSource.setDefault(v);
            }else {
                dynamicDataSource.add(k,v);
            }
        });
        return dynamicDataSource;
    }

    /**
     * 配置自定义JSON结果
     * @see CustomJsonValueProvider
     */
    @Bean
    public CustomJsonValueProvider customJsonValueProvider(){
        return new CustomJsonValueProvider();
    }

    /**
     * 自定义UI界面鉴权
     * @see CustomAuthorizationInterceptor
     */
    @Bean
    @Lazy
    @ConditionalOnMissingBean(AuthorizationInterceptor.class)
    public AuthorizationInterceptor customAuthorizationInterceptor(){
        return new CustomAuthorizationInterceptor(1);
    }

}
