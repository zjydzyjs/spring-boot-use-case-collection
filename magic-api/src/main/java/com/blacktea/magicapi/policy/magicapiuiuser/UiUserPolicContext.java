package com.blacktea.magicapi.policy.magicapiuiuser;

import com.blacktea.magicapi.entites.domain.MguMagicUiUser;
import com.blacktea.magicapi.entites.dto.UiUserDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ssssssss.magicapi.exception.MagicLoginException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @description:
 * @author: black tea
 * @date: 2022/1/1 23:28
 */
@AllArgsConstructor
@Slf4j
public class UiUserPolicContext {

    private UiUserPolicy policy;

    /**
     * 默认使用 Map 进行存储 k,v -> token(UUID),存储用户信息
     * @see UiUserMapPolicy
     */
    public static UiUserPolicContext of(){
        return new UiUserPolicContext(new UiUserMapPolicy());
    }

    public static UiUserPolicContext of(UiUserPolicy policy){
        return new UiUserPolicContext(
                Optional.ofNullable(policy).orElseGet(UiUserMapPolicy::new)
        );
    }

    public String buildToken(UiUserDto dto){
        return this.policy.buildToken(dto);
    }

    public UiUserDto getUser(String token){
        return this.policy.getUser(token);
    }

    public List<UiUserDto> users(){
        return this.policy.users();
    }

    public void removeByToken(String token){
        this.policy.removeByToken(token);
    }

    public List<UiUserDto> getByUsername(String username){
       return this.policy.getByUsername(username);
    }

    public void limitLogin(String username,Integer limitLogin) throws MagicLoginException {
        this.policy.limitLogin(username,limitLogin,this.getByUsername(username).size());
    }

    public void checkLockAccount(MguMagicUiUser user) throws MagicLoginException {
        this.policy.checkLockAccount(user);
    }

    public void ipBlacklist(String ipBlackConfig) throws MagicLoginException {
        this.policy.hasIpBlacklist(ipBlackConfig);
    }

    public void ipWhitelists(String ipWhiteConfig) throws MagicLoginException {
        this.policy.hasIpWhitelists(ipWhiteConfig);
    }

    public String getWhitelist(MguMagicUiUser user){
        return this.policy.getWhitelist(user);
    }

    public void putWhitelist(MguMagicUiUser user, List<String> ips){
        this.policy.putWhitelist(user,ips);
    }

    public void removeByIpWhitelist(MguMagicUiUser user, List<String> matchIps){
        this.policy.removeByIpWhitelist(user,matchIps);
    }

    public void removeAllWhitelist(MguMagicUiUser user){
        this.policy.removeAllWhitelist(user);
    }

    public void replaceWhitelist(MguMagicUiUser user, Map<String,String> replaceIpMap){
        this.policy.replaceWhitelist(user,replaceIpMap);
    }

    String getBlacklist(MguMagicUiUser user){
        return this.policy.getBlacklist(user);
    }

    public void putBlacklist(MguMagicUiUser user, List<String> ips){
        this.policy.putBlacklist(user,ips);
    }

    public void removeByIpBlacklist(MguMagicUiUser user, List<String> matchIps){
        this.policy.removeByIpBlacklist(user,matchIps);
    }

    public void removeAllBlacklist(MguMagicUiUser user){
        this.policy.removeAllBlacklist(user);
    }

    public void replaceBlacklist(MguMagicUiUser user, Map<String,String> replaceIpMap){
        this.policy.replaceBlacklist(user,replaceIpMap);
    }

    /**
     * magic-api ui用户登录时共同校验是否可以登录的条件
     * @param user ui用户
     * @param limitLogin 限制账号在线人数
     * @throws MagicLoginException
     */
    public void checkCommonLogin(MguMagicUiUser user,Integer limitLogin) throws MagicLoginException {
        log.info("当前请求的IP:{}",this.policy.getIp());
        // ip是否在黑名单
        this.ipBlacklist(this.policy.getBlacklist(user));
        // ip是否在白名单
        this.ipWhitelists(this.policy.getWhitelist(user));
        // 检查账号是否被锁定
        this.checkLockAccount(user);
        // 同个账号是否超出限制在线人数
        this.limitLogin(user.getUsername(),limitLogin);
    }

}
