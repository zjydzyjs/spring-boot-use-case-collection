package com.blacktea.magicapi.policy.exitvalue;

import com.blacktea.magicapi.base.BaseConstant;
import org.ssssssss.magicapi.model.Constants;
import org.ssssssss.magicapi.model.RequestEntity;
import org.ssssssss.magicapi.provider.ResultProvider;
import org.ssssssss.script.functions.ObjectConvertExtension;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @description: 对象处理
 * @author: black tea
 * @date: 2021/12/31 15:12
 */
public class ObjectExitValue0Policy implements ExitValue0Policy{

    @Override
    public Object buildResult(Object[] values, RequestEntity requestEntity, ResultProvider provider) {
        Map<String,Object> map = (HashMap) values[0];
        Integer code = ObjectConvertExtension.asInt(map.get("code"), Constants.RESPONSE_CODE_SUCCESS);
        String message = Objects.toString(map.get("message"), BaseConstant.RESULT_SUCCESS_MESSAGE);
        Object dataResult = map.getOrDefault("data", null);
        return provider.buildResult(requestEntity,code,message,dataResult);
    }
}
