package com.blacktea.magicapi.policy.exitvalue;

import org.ssssssss.magicapi.model.RequestEntity;
import org.ssssssss.magicapi.provider.ResultProvider;

/**
 * @description:
 * @author: black tea
 * @date: 2021/12/31 14:54
 */
@FunctionalInterface
public interface ExitValue0Policy {

    /**
     * 生成自定义返回的json对象内的code、message、data
     * @param values {@link ResultProvider}.buildResult 方法入参
     * @param requestEntity {@link ResultProvider}.buildResult 方法入参
     * @param provider {@link ResultProvider} 实现类对象
     * @return Object
     */
    Object buildResult(Object[] values, RequestEntity requestEntity, ResultProvider provider);
}
