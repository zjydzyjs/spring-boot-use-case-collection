package com.blacktea.magicapi.policy.exitvalue;

import com.blacktea.magicapi.base.ResultCode;
import org.ssssssss.magicapi.model.RequestEntity;
import org.ssssssss.magicapi.provider.ResultProvider;

/**
 * @description: 枚举处理
 * @author: black tea
 * @date: 2021/12/31 15:00
 */
public class EnumExitValue0Policy implements ExitValue0Policy{

    @Override
    public Object buildResult(Object[] values, RequestEntity requestEntity, ResultProvider provider) {
        ResultCode resultCode = (ResultCode) values[0];
        return provider.buildResult(requestEntity,resultCode.getCode(),resultCode.getMessage(),values.length > 1 ? values[1] : null);
    }

}
