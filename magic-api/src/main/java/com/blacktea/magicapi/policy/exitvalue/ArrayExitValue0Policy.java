package com.blacktea.magicapi.policy.exitvalue;

import com.blacktea.magicapi.base.BaseConstant;
import org.ssssssss.magicapi.model.Constants;
import org.ssssssss.magicapi.model.RequestEntity;
import org.ssssssss.magicapi.provider.ResultProvider;
import org.ssssssss.script.functions.ObjectConvertExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @description: 数组处理
 * @author: black tea
 * @date: 2021/12/31 15:10
 */
public class ArrayExitValue0Policy implements ExitValue0Policy{

    @Override
    public Object buildResult(Object[] values, RequestEntity requestEntity, ResultProvider provider) {
        List<Object> arrayList = (ArrayList) values[0];
        Integer code = ObjectConvertExtension.asInt(arrayList.get(0), Constants.RESPONSE_CODE_SUCCESS);
        int size = arrayList.size();
        String message = size > 1 ? Objects.toString(arrayList.get(1), BaseConstant.RESULT_SUCCESS_MESSAGE) : BaseConstant.RESULT_SUCCESS_MESSAGE;
        return provider.buildResult(requestEntity,code,message,size > 2 ? arrayList.get(2) : null);
    }
}
