package com.blacktea.magicapi.model;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitOperations;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.config.MagicModule;
import org.ssssssss.script.annotation.Comment;

/**
 * @description: 自定义模块之 RabbitMQ
 * @author: black tea
 * @date: 2021/12/25 14:01
 */
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "spring.rabbitmq",value = {"host"})
@Component
public class RabbitMqModel implements MagicModule {

    private final RabbitOperations rabbitOperations;

    @Override
    public String getModuleName() {
        return "rabbitmq";
    }

    @Comment("获取RabbitOperations")
    public RabbitOperations getRabbit(){
        return this.rabbitOperations;
    }
}
