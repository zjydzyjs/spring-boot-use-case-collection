package com.blacktea.magicapi;


import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * @description:
 * @author: black tea
 * @date: 2021/7/28 9:56
 */
// 演示例子，执行 main 方法控制台输入模块表名回车自动生成对应项目目录中
public class CodeGenerator {
    public static void main(String[] args) {
        GlobalConfig globalConfig = new GlobalConfig();
        String property = System.getProperty("user.dir");
        property = property+"/magic-api";
        globalConfig.setOutputDir(property + "/src/main/java");
        globalConfig.setAuthor("blacktea");
        globalConfig.setOpen(false);
        globalConfig.setActiveRecord(true);
        globalConfig.setFileOverride(true);
        globalConfig.setServiceName("%sService");
        globalConfig.setIdType(IdType.ASSIGN_ID).setBaseResultMap(true).setBaseColumnList(true);



        //2. 数据源配置
        DataSourceConfig dsConfig = new DataSourceConfig();
        dsConfig.setDbType(DbType.MYSQL)  // 设置数据库类型
                .setDriverName("com.mysql.jdbc.Driver")
                .setUrl("jdbc:mysql://127.0.0.1:3306/magic-api-test?useUnicode=true&zeroDateTimeBehavior=convertToNull&autoReconnect=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai")
                .setUsername("root")
                .setPassword("mysql8.0");

        //3. 策略配置
        StrategyConfig stConfig = new StrategyConfig();
        stConfig.setCapitalMode(true) //全局大写命名
                .setEntityLombokModel(true)
                .setTablePrefix("")
                .setNaming(NamingStrategy.underline_to_camel) // 数据库表映射到实体的命名策略
                .setInclude("mgu_magic_ui_permission","mgu_magic_ui_role","mgu_magic_ui_role_permission","mgu_magic_ui_user","mgu_magic_ui_user_role");  // 生成的表
        stConfig.setSuperEntityClass("com.blacktea.magicapi.entites.BasicObject");
        // 写于父类中的公共字段
        stConfig.setSuperEntityColumns("id");
        //4. 包名策略配置
        PackageConfig pkConfig = new PackageConfig();
        pkConfig.setParent("com.blacktea.magicapi")
                .setMapper("dao.mapper")
                .setService("service")
                .setController("controller")
                .setEntity("entites.domain")
                .setXml("dao.mapper");

        //5. 整合配置
        AutoGenerator ag = new AutoGenerator();

        ag.setGlobalConfig(globalConfig)
                .setDataSource(dsConfig)
                .setStrategy(stConfig)
                .setPackageInfo(pkConfig);

        //6. 执行
        ag.execute();
    }


}
