package com.blacktea.rabbitmq.springbootrabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 备份和警告 配置
 * @description:
 * @author: black tea
 * @date: 2022/6/6 21:17
 */
@Configuration
public class BackupWarningConfig {

    /** 备份队列 **/
    public final static String BACKUP_QUEUE_NAME = "backup_queue";

    /** 备份交换机 **/
    public final static String BACKUP_EXCHANGE_NAME = "backup_exchange";

    /** 警告队列 **/
    public final static String WARNING_QUEUE_NAME = "warning_queue";

    @Bean
    public Exchange backupExchange(){

        return ExchangeBuilder
                .fanoutExchange(BACKUP_EXCHANGE_NAME)
                .build();
    }

    @Bean
    public Queue backupQueue(){

        return QueueBuilder
                .durable(BACKUP_QUEUE_NAME)
                .build();
    }

    @Bean
    public Queue warningQueue(){

        return QueueBuilder
                .durable(WARNING_QUEUE_NAME)
                .build();
    }

    @Bean
    public Binding backupQueueBindingBackupExchange(@Qualifier("backupQueue") Queue backupQueue,
                                                    @Qualifier("backupExchange") Exchange backupExchange){

        return BindingBuilder
                .bind(backupQueue)
                .to(backupExchange)
                .with("")
                .noargs();
    }

    @Bean
    public Binding warningQueueBindingBackupExchange(@Qualifier("warningQueue") Queue warningQueue,
                                                    @Qualifier("backupExchange") Exchange backupExchange){

        return BindingBuilder
                .bind(warningQueue)
                .to(backupExchange)
                .with("")
                .noargs();
    }
}
