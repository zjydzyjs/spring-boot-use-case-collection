package com.blacktea.rabbitmq.springbootrabbitmq.consumer;

import cn.hutool.core.date.DateUtil;
import com.blacktea.rabbitmq.springbootrabbitmq.config.PriorityConfig;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 优先级消费者
 * @description:
 * @author: black tea
 * @date: 2022/6/6 22:41
 */
@Component
@Slf4j
public class PriorityConsumer {

    @RabbitListener(queues = {PriorityConfig.PRIORITY_QUEUE_NAME})
    public void receivePriorityQueue(Message message, Channel channel){
        log.info("当前时间:{},优先级消费者，接收到消息:{}", DateUtil.now(), new String(message.getBody()));
    }


}
