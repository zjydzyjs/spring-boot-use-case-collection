package com.blacktea.rabbitmq.springbootrabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 镜像配置
 * @description:
 * @author: black tea
 * @date: 2022/6/8 22:06
 */
@Configuration
public class MirriorConfig {

    /** 交换机 **/
    public final static String MIRRIOR_EXCHANGE_NAME = "mirrior_exchange";

    /** 队列 **/
    public final static String MIRRIOR_QUEUE_NAME = "mirrior_queue";

    /** RoutingKey **/
    public final static String MIRRIOR_ROUTING_KEY = "mirrior";

    @Bean
    public Queue mirriorQueue(){

        return QueueBuilder
                .durable(MIRRIOR_QUEUE_NAME)
                .build();
    }

    @Bean
    public Exchange mirriorExchange(){

        return ExchangeBuilder
                .directExchange(MIRRIOR_EXCHANGE_NAME)
                .build();
    }

    @Bean
    public Binding mirriorQueueBindingMirriorExchange(@Qualifier("mirriorQueue") Queue mirriorQueue,
                                                      @Qualifier("mirriorExchange") Exchange mirriorExchange){

        return BindingBuilder
                .bind(mirriorQueue)
                .to(mirriorExchange)
                .with(MIRRIOR_ROUTING_KEY)
                .noargs();
    }
}
