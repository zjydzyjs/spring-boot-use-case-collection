package com.blacktea.rabbitmq.springbootrabbitmq.consumer;

import cn.hutool.core.date.DateUtil;
import com.blacktea.rabbitmq.springbootrabbitmq.config.TtlQueueConfig;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 队列TTL 消费者
 * @description:
 * @author: black tea
 * @date: 2022/6/5 17:05
 */
@Slf4j
@Component
public class DeadLetterConsumer {

    @RabbitListener(queues = {TtlQueueConfig.DEAD_LETTER_QUEUE_D})
    public void receiveD(Message message, Channel channel){
        String msg = new String(message.getBody());
        log.info("当前时间:{},收到死信队列的消息:{}", DateUtil.now(), msg);

    }


}
