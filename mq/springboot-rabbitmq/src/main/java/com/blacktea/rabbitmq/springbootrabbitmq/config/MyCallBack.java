package com.blacktea.rabbitmq.springbootrabbitmq.config;

import cn.hutool.core.lang.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * RabbitMQ 确认回调接口实现
 * @description:
 * @author: black tea
 * @date: 2022/6/6 20:39
 */
@Slf4j
@Component
public class MyCallBack implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnCallback{

    public MyCallBack(RabbitTemplate rabbitTemplate) {
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.setReturnCallback(this);
    }

    /**
     * 交换机确认回调方法
     *  1、发消息 交换机接收到了 回调
     *    1.1、correlationData 保存回调消息的ID及相关信息
     *    1.2、交换机收到消息 ack = true
     *    1.3、cause null
     *  2、发消息 交换机接收失败了 回调
     *    2.1、correlationData 保存回调消息的ID及相关信息
     *    2.2、交换机收到消息 ack = false
     *    2.3、cause 失败的原因
     *
     * @param correlationData 保存回调消息的ID及相关信息
     * @param ack 交换机收到消息
     * @param cause 失败的原因
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {

        String id = Validator.isNotNull(correlationData) ? correlationData.getId() : "";
        String msg = "交换机{}收到id为:{}的消息";
        if (ack){
            log.info(msg, "已经", id);
        }else {
            log.warn(msg+",原因是:{}", "还未", id, cause);
        }
    }

    /**
     * 可以在当消息传递过程中不可达目的地时将消息返回给生产者
     * 只有不可达目的地时才可以进行回退
     *
     * @param message
     * @param replyCode
     * @param replyText
     * @param exchange
     * @param routingKey
     */
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {

        log.warn("消息:{},被交换机{}退回,退回原因:{},路由Key:{}",
                new String(message.getBody()), exchange, replyText, routingKey);
    }


}
