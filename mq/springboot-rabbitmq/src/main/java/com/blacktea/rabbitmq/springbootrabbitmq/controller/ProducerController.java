package com.blacktea.rabbitmq.springbootrabbitmq.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.blacktea.rabbitmq.springbootrabbitmq.common.Constant;
import com.blacktea.rabbitmq.springbootrabbitmq.config.ConfirmConfig;
import com.blacktea.rabbitmq.springbootrabbitmq.config.LazyQueueConfig;
import com.blacktea.rabbitmq.springbootrabbitmq.config.MirriorConfig;
import com.blacktea.rabbitmq.springbootrabbitmq.config.PriorityConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: black tea
 * @date: 2022/6/6 20:26
 */
@RestController
@RequestMapping("/confirm")
@RequiredArgsConstructor
@Slf4j
public class ProducerController {

    private final RabbitTemplate rabbitTemplate;

    @GetMapping("sendMsg/{message}")
    public void sendMsg(@PathVariable("message") String message){

        log.info("当前时间为:{},发送消息:{}", DateUtil.now(), message);
        CorrelationData correlationData = new CorrelationData(IdUtil.simpleUUID());
        rabbitTemplate.convertAndSend(
                ConfirmConfig.CONFIRM_EXCHANGE_NAME,
                ConfirmConfig.CONFIRM_ROUTING_KEY,
                message + ConfirmConfig.CONFIRM_ROUTING_KEY,
                correlationData
        );
        rabbitTemplate.convertAndSend(
                ConfirmConfig.CONFIRM_EXCHANGE_NAME,
                ConfirmConfig.CONFIRM_ROUTING_KEY+"-two",
                message + ConfirmConfig.CONFIRM_ROUTING_KEY+"-two",
                correlationData
        );
    }

    // TODO rabbitmq 幂等性待编写测试


    @GetMapping("sendMsg/5/{message}")
    public void sendMsg5(@PathVariable("message") String message){

        String priorityExchangeName = PriorityConfig.PRIORITY_EXCHANGE_NAME;
        String priorityRoutingKey = PriorityConfig.PRIORITY_ROUTING_KEY;
        for (int i = 1; i <= 10; i++) {
            String sendMsg = String.format("这是第%s条消息,消息内容:%s", i, message);
            log.info("当前时间为:{},发送消息:{}", DateUtil.now(), sendMsg);
            String uuid = IdUtil.simpleUUID();
            CorrelationData correlationData = new CorrelationData(uuid);
            if (i == 9){
                priorityRoutingKey = priorityRoutingKey+1;
            }else {
                priorityRoutingKey = PriorityConfig.PRIORITY_ROUTING_KEY;
            }
            int finalI1 = i;
            rabbitTemplate.convertAndSend(
                    priorityExchangeName,
                    priorityRoutingKey,
                    sendMsg,
                    (mqMsg) -> {
                        if ( finalI1 == 5 ){
                            Integer priority = Constant.RabbitMqQueuePriority.FIVE.getPriority();
                            mqMsg.getMessageProperties().setPriority(priority);
                        }
                        return mqMsg;
                    },
                    correlationData
            );
        }
    }

    @GetMapping("sendMsg/lazy/{message}/{total}")
    public void sendMsgLazy(@PathVariable("message") String message, @PathVariable("total") Integer total){

        String lazyExchangeName = LazyQueueConfig.LAZY_EXCHANGE_NAME;
        String lazyRoutingKey = LazyQueueConfig.LAZY_ROUTING_KEY;
        for (int index = 1; index <= total; index++) {
            String sendMsg = String.format("这是第%s条消息,消息内容:%s", index, message);
            log.info("当前时间为:{},发送消息:{}", DateUtil.now(), sendMsg);
            String uuid = IdUtil.simpleUUID();
            CorrelationData correlationData = new CorrelationData(uuid);
            rabbitTemplate.convertAndSend(
                    lazyExchangeName,
                    lazyRoutingKey,
                    sendMsg,
                    correlationData
            );
        }
    }

    @GetMapping("sendMsg/mirror/{message}")
    public void sendMsgMirror(@PathVariable("message") String message){

        String exchangeName = MirriorConfig.MIRRIOR_EXCHANGE_NAME;
        String routingKey = MirriorConfig.MIRRIOR_ROUTING_KEY;
        String sendMsg = String.format("消息内容:%s", message);
        log.info("当前时间为:{},发送消息:{}", DateUtil.now(), sendMsg);
        String uuid = IdUtil.simpleUUID();
        CorrelationData correlationData = new CorrelationData(uuid);
        rabbitTemplate.convertAndSend(
                exchangeName,
                routingKey,
                sendMsg,
                correlationData
        );

    }
}
