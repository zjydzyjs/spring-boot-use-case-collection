package com.blacktea.rabbitmq.springbootrabbitmq.config;

import com.blacktea.rabbitmq.springbootrabbitmq.common.Constant;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 优先级配置
 * @description:
 * @author: black tea
 * @date: 2022/6/6 22:07
 */
@Configuration
public class PriorityConfig {

    /** 优先级队列 **/
    public final static String PRIORITY_QUEUE_NAME = "priority_queue";

    /** 优先级交换机 **/
    public final static String PRIORITY_EXCHANGE_NAME = "priority_exchange";

    /** RoutingKey **/
    public final static String PRIORITY_ROUTING_KEY = "priority_routingKey";

    @Bean
    public Queue priorityQueue(){

        return QueueBuilder
                .durable(PRIORITY_QUEUE_NAME)
                .maxPriority(Constant.DEFAULT_QUEUE_MAX_PRIORITY)
                .build();
    }

    @Bean
    public Exchange priorityExchange(){

        return ExchangeBuilder
                .directExchange(PRIORITY_EXCHANGE_NAME)
                .build();
    }

    @Bean
    public Binding priorityQueueBindingPriorityExchange(@Qualifier("priorityQueue") Queue priorityQueue,
                                                        @Qualifier("priorityExchange") Exchange priorityExchange){

        return BindingBuilder
                .bind(priorityQueue)
                .to(priorityExchange)
                .with(PRIORITY_ROUTING_KEY)
                .noargs();
    }


}
