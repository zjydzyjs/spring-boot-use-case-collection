package com.blacktea.rabbitmq.springbootrabbitmq.consumer;

import cn.hutool.core.date.DateUtil;
import com.blacktea.rabbitmq.springbootrabbitmq.config.MirriorConfig;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: black tea
 * @date: 2022/6/8 22:11
 */
@Component
@Slf4j
public class MirriorConsumer {

    @RabbitListener(queues = {MirriorConfig.MIRRIOR_QUEUE_NAME})
    public void  receiveMirriorQueue(Message message, Channel channel){

        log.info("当前时间:{},镜像消费者，接收到消息:{}", DateUtil.now(), new String(message.getBody()));
    }
}
