package com.blacktea.rabbitmq.springbootrabbitmq.consumer;

import cn.hutool.core.date.DateUtil;
import com.blacktea.rabbitmq.springbootrabbitmq.config.ConfirmConfig;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 接收消息
 *
 * @description:
 * @author: black tea
 * @date: 2022/6/6 20:30
 */
@Component
@Slf4j
public class Consumer {

    @RabbitListener(queues = {ConfirmConfig.CONFIRM_QUEUE_NAME})
    public void receiveConfirmMessage(Message message, Channel channel){
        String msg = new String(message.getBody());
        log.info("当前时间:{},接收到队列{}的消息:{}", DateUtil.now(), ConfirmConfig.CONFIRM_QUEUE_NAME, msg);
    }

}
