package com.blacktea.rabbitmq.springbootrabbitmq.consumer;

import cn.hutool.core.date.DateUtil;
import com.blacktea.rabbitmq.springbootrabbitmq.config.DelayedQueueConfig;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

/**
 * 基于插件的延迟消息
 * @description:
 * @author: black tea
 * @date: 2022/6/5 20:23
 */
//@Component
@Slf4j
public class DelayedQueueConsumer {

    @RabbitListener(queues = {DelayedQueueConfig.DELAYED_QUEUE_NAME})
    public void receiveDelayedQueue(Message message, Channel channel){
        String msg = new String(message.getBody());
        log.info("当前时间:{},收到插件延迟队列的消息:{}", DateUtil.now(), msg);
    }
}
