package com.blacktea.rabbitmq.springbootrabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 惰性队列 配置
 * @description:
 * @author: black tea
 * @date: 2022/6/7 22:53
 */
@Configuration
public class LazyQueueConfig {

    /** 惰性队列 **/
    public final static String LAZY_QUEUE_NAME = "lazy_queue";

    /** 惰性队列交换机 **/
    public final static String LAZY_EXCHANGE_NAME = "lazy_exchange";

    /** RoutingKey **/
    public final static String LAZY_ROUTING_KEY = "lazy_routingKey";

    @Bean
    public Queue lazyQueue(){

        return QueueBuilder
                .durable(LAZY_QUEUE_NAME)
                .lazy()
                .build();
    }

    @Bean
    public Exchange lazyExchange(){

        return ExchangeBuilder
                .directExchange(LAZY_EXCHANGE_NAME)
                .build();
    }

    @Bean
    public Binding lazyQueueBindingLazyExchange(@Qualifier("lazyQueue") Queue lazyQueue,
                                                        @Qualifier("lazyExchange") Exchange lazyExchange){

        return BindingBuilder
                .bind(lazyQueue)
                .to(lazyExchange)
                .with(LAZY_ROUTING_KEY)
                .noargs();
    }
}
