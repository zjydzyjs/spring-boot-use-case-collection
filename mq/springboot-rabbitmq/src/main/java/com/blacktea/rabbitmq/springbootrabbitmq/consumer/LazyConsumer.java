package com.blacktea.rabbitmq.springbootrabbitmq.consumer;

import cn.hutool.core.date.DateUtil;
import com.blacktea.rabbitmq.springbootrabbitmq.config.LazyQueueConfig;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 惰性队列消费者
 * @description:
 * @author: black tea
 * @date: 2022/6/7 22:58
 */
@Component
@Slf4j
public class LazyConsumer {

    @RabbitListener(queues = {LazyQueueConfig.LAZY_QUEUE_NAME})
    public void receiveLazyQueue(Message message, Channel channel){

        log.info("惰性队列消费者接收到消息,当前时间:{},消息内容为:{}", DateUtil.now(), new String(message.getBody()));
    }

}
