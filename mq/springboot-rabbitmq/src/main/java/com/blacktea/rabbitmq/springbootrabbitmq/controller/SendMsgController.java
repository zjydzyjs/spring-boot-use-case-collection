package com.blacktea.rabbitmq.springbootrabbitmq.controller;

import cn.hutool.core.date.DateUtil;
import com.blacktea.rabbitmq.springbootrabbitmq.config.DelayedQueueConfig;
import com.blacktea.rabbitmq.springbootrabbitmq.config.TtlQueueConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 发送消息
 * @description:
 * @author: black tea
 * @date: 2022/6/5 16:54
 */
@RestController
@RequestMapping("/sendMsg")
@RequiredArgsConstructor
@Slf4j
public class SendMsgController {

    private final RabbitTemplate rabbitTemplate;

    /**
     * 发送10s and 40s延迟消息
     */
    @GetMapping("ttl/{message}")
    public void setTtlMsg(@PathVariable("message") String message){
        log.info("当前时间:{},发送一条信息给两个TTL队列,消息:{}", DateUtil.now(), message);
        String msg = "消息来自ttl为%s的队列,消息内容: %s";
        rabbitTemplate.convertAndSend(TtlQueueConfig.EXCHANGE_X,
                TtlQueueConfig.QUEUE_A_BINDING_EXCHANGE_X_ROUTING_KEY,
                String.format(msg, "10s", message)
        );
        rabbitTemplate.convertAndSend(TtlQueueConfig.EXCHANGE_X,
                TtlQueueConfig.QUEUE_B_BINDING_EXCHANGE_X_ROUTING_KEY,
                String.format(msg, "40s", message)
        );
    }

    /**
     * 发送自定义s延迟消息
     */
    @GetMapping("ttl/{ttl}/{message}")
    public void setTtlMsg(@PathVariable("ttl") String ttl,
                          @PathVariable("message") String message){
        log.info("当前时间:{},发送一条时长{}毫秒,TTL队列消息:{}",
                DateUtil.now(), ttl, message);
        String msg = "消息来自ttl为%s毫秒的队列,消息内容: %s";
        rabbitTemplate.convertAndSend(TtlQueueConfig.EXCHANGE_X,
                TtlQueueConfig.QUEUE_C_BINDING_EXCHANGE_X_ROUTING_KEY,
                String.format(msg, ttl, message),
                (mqMsg)-> {
                    // 发送延迟消息时，设置指定的延迟时长
                    mqMsg.getMessageProperties().setExpiration(ttl);
                    return mqMsg;
                }
        );
    }

    /**
     * 基于插件发送延迟消息
     */
    @GetMapping("delayed/{ttl}/{message}")
    public void delayed(@PathVariable("ttl") Integer ttl,
                        @PathVariable("message") String message){
        log.info("当前时间:{},发送一条时长{}毫秒信息给插件延迟队列:{}",
                DateUtil.now(), ttl, message);
        String msg = "消息来自ttl为%s毫秒的插件延迟队列,消息内容: %s";
        rabbitTemplate.convertAndSend(DelayedQueueConfig.DELAYED_EXCHANGE_NAME,
                DelayedQueueConfig.DELAYED_ROUTING_KEY,
                String.format(msg, ttl, message),
                (mqMsg)-> {
                    // 发送延迟消息时，设置指定的延迟时长
                    mqMsg.getMessageProperties().setDelay(ttl);
                    return mqMsg;
                }
        );
    }

}
