package com.blacktea.rabbitmq.springbootrabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 发布确认配置类
 *
 * @description:
 * @author: black tea
 * @date: 2022/6/6 20:10
 */
@Configuration
public class ConfirmConfig {

    /** 交换机 **/
    public final static String CONFIRM_EXCHANGE_NAME = "confirm_exchange";

    /** 队列 **/
    public final static String CONFIRM_QUEUE_NAME = "confirm_queue";

    /** RoutingKey **/
    public final static String CONFIRM_ROUTING_KEY = "key1";

    @Bean
    public Exchange confirmExchange(){


        return ExchangeBuilder
                .directExchange(CONFIRM_EXCHANGE_NAME)
                // 转发到备份交换机,注意:如果备份了就不会进行回退消息了
                .withArgument("alternate-exchange", BackupWarningConfig.BACKUP_EXCHANGE_NAME)
                .build();
    }

    @Bean
    public Queue confirmQueue(){

        return QueueBuilder
                .durable(CONFIRM_QUEUE_NAME)
                .build();
    }

    @Bean
    public Binding confirmQueueBindingConfirmExchange(@Qualifier("confirmQueue") Queue confirmQueue,
                                                      @Qualifier("confirmExchange") Exchange confirmExchange){

        return BindingBuilder
                .bind(confirmQueue)
                .to(confirmExchange)
                .with(CONFIRM_ROUTING_KEY)
                .noargs();
    }

}
