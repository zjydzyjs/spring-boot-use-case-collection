package com.blacktea.rabbitmq.springbootrabbitmq.consumer;

import com.blacktea.rabbitmq.springbootrabbitmq.config.BackupWarningConfig;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 报警队列消费者
 * @description:
 * @author: black tea
 * @date: 2022/6/6 21:40
 */
@Component
@Slf4j
public class WaringConsumer {

    @RabbitListener(queues = {BackupWarningConfig.WARNING_QUEUE_NAME})
    public void receiveWaringMsg(Message message, Channel channel){

        String msg = new String(message.getBody());
        log.error("发现不可路由报警消息:{}", msg);
    }
}
