package com.blacktea.rabbitmq.springbootrabbitmq.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @description:
 * @author: black tea
 * @date: 2022/6/6 22:14
 */
public interface Constant {

    /**
     * rabbitmq 队列 优先级范围为 0-255
     */
    @AllArgsConstructor
    @Getter
    enum RabbitMqQueuePriority {
        /** 最小优先级 **/
        MIN(0),
        ONE(1),
        TWO(2),
        THREE(3),
        FOUR(4),
        FIVE(5),
        SIX(6),
        SEVEN(7),
        EIGHT(8),
        NINE(9),
        TEN(10),
        /** 最大优先级 **/
        MAX(255)
        ;
        /** 优先级值 **/
        private Integer priority;
    }

    /** 默认限制队列最大优先级为 10,以免浪费内存 **/
    Integer DEFAULT_QUEUE_MAX_PRIORITY = RabbitMqQueuePriority.TEN.getPriority();

}
