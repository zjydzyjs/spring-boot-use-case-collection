package com.blacktea.webrtc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: black tea
 * @date: 2023/3/15 19:59
 */
@RequestMapping("/test")
@RestController
public class TestController {

    @GetMapping
    public String test(){
        return "ok";
    }
}
